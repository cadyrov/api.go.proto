//nolint
package cache

import (
	"fmt"
	"testing"
	"time"

	"api.go.proto/internal/domain"
)

func TestCache(t *testing.T) {
	c := InitCache()
	for i := 0; i < 10000; i++ {

		func(i int) {
			c.UpdateUser(&domain.User{ID: i,
				Email: fmt.Sprintf("emailUI@%d", i),
			})
		}(i)
	}

	timeTest := time.Now()

	for i := 400; i < 412; i++ {
		fmt.Println(c.UserByID(&domain.User{ID: i}))
		fmt.Println(c.FindUserByEmail(fmt.Sprintf("emailUI@%d", i)))
	}

	fmt.Println(time.Since(timeTest))
}
