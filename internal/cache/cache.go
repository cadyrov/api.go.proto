package cache

import (
	"strings"
	"sync"

	"api.go.proto/internal/domain"
)

type Cache interface {
	UserByID(*domain.User) *domain.User
	AccountByID(*domain.Account) *domain.Account
	UpdateUser(*domain.User)
	UpdateAccount(*domain.Account)
	FindUserByEmail(email string) *domain.User
}

func InitCache() Cache {
	l := localCache{}

	l.emails = make(map[string]int)
	l.users = make(map[int]*domain.User)
	l.accounts = make(map[int]*domain.Account)

	return &l
}

type localCache struct {
	mu       sync.RWMutex
	emails   map[string]int
	users    map[int]*domain.User
	accounts map[int]*domain.Account
}

func (l *localCache) UserByID(u *domain.User) *domain.User {
	if u == nil {
		return nil
	}

	l.mu.RLock()
	defer l.mu.RUnlock()

	if usr := l.users[u.ID]; usr != nil {
		res := *usr

		return &res
	}

	return nil
}

func (l *localCache) UpdateUser(u *domain.User) {
	if u == nil {
		return
	}

	l.mu.Lock()
	defer l.mu.Unlock()

	l.users[u.ID] = u
	l.emails[strings.ToLower(u.Email)] = u.ID
}

func (l *localCache) AccountByID(a *domain.Account) *domain.Account {
	if a == nil {
		return nil
	}

	l.mu.RLock()
	defer l.mu.RUnlock()

	if acc := l.accounts[a.ID]; acc != nil {
		res := *acc

		return &res
	}

	return nil
}

func (l *localCache) UpdateAccount(a *domain.Account) {
	if a == nil {
		return
	}

	l.mu.Lock()
	defer l.mu.Unlock()

	l.accounts[a.ID] = a
}

func (l *localCache) FindUserByEmail(email string) *domain.User {
	eml := strings.ToLower(email)

	l.mu.RLock()
	v, ok := l.emails[eml]
	l.mu.RUnlock()

	if ok {
		return l.UserByID(&domain.User{ID: v})
	}

	return nil
}
