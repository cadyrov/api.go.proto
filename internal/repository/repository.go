package repository

import (
	"api.go.proto/internal/repository/access"
	"api.go.proto/internal/repository/psql"
	"api.go.proto/tools/config"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
)

type Repository struct {
	PSQLQueryer gopsql.Queryer
	AccountAI   access.AccountAI
	UserAI      access.UserAI
	FileAI      access.FileAI
}

type Config struct {
	DB gopsql.Config
}

func NewConfig(cn config.PSQL) Config {
	return Config{
		DB: gopsql.Config{
			Host:           cn.Host,
			Port:           cn.Port,
			UserName:       cn.UserName,
			DBName:         cn.DBName,
			Password:       cn.Password,
			SslMode:        cn.SslMode,
			Binary:         cn.Binary,
			MaxConnections: cn.MaxConnections,
			ConnectionIdle: cn.ConnectionIdle,
		},
	}
}

func New(config Config, isTransactional bool) (*Repository, goerr.IError) {
	r := Repository{}

	DB, e := config.DB.Connect()
	if e != nil {
		return nil, e
	}

	r.PSQLQueryer = DB

	if isTransactional {
		r.PSQLQueryer, e = DB.Begin()
		if e != nil {
			return nil, e
		}
	}

	r.plSQLCreate()

	return &r, nil
}

func (r *Repository) plSQLCreate() {
	r.AccountAI = psql.NewAccount(r.PSQLQueryer)
	r.UserAI = psql.NewUser(r.PSQLQueryer)
	r.FileAI = psql.NewFile(r.PSQLQueryer)
}

func (r *Repository) Begin() (access.Transacter, goerr.IError) {
	rt := realTransaction{}

	if r.isTransactional() {
		rt.Tx = r.PSQLQueryer.(*gopsql.Tx)
		rt.unique = true
	} else {
		if r.PSQLQueryer == nil {
			return nil, nil
		}

		txPSQL, e := r.PSQLQueryer.(*gopsql.DB).Begin()
		if e != nil {
			return nil, e
		}

		rt.Tx = txPSQL
	}

	return &rt, nil
}

func (r *Repository) isTransactional() bool {
	_, ok := r.PSQLQueryer.(*gopsql.Tx)

	return ok
}
