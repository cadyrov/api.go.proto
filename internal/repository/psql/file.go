package psql

import (
	"database/sql"
	"strings"

	"api.go.proto/internal/domain"
	"api.go.proto/internal/repository/access"
	"api.go.proto/utils"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
	"github.com/lib/pq"
)

type FileDAO struct {
	gopsql.Queryer
}

func NewFile(db gopsql.Queryer) *FileDAO {
	return &FileDAO{Queryer: db}
}

func (fileDAO *FileDAO) Table() string {
	return "public.file"
}

func (fileDAO *FileDAO) Columns() []string {
	return []string{"id", "name", "roles", "created_at", "user_id", "account_id", "extension", "link", "size"}
}

func (fileDAO *FileDAO) Values(d *domain.File) (values []interface{}) {
	return append(values, &d.ID, &d.Name, pq.Array(&d.Roles),
		&d.CreatedAt, &d.UserID, &d.AccountID, &d.Extension, &d.Link, &d.Size)
}

func (fileDAO *FileDAO) ByID(d *domain.File, tx access.Transacter) (*domain.File,
	goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(fileDAO.Columns(), ","))

	queryBuilder.Add("FROM " + fileDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = fileDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	fileEntity, e := fileDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	if fileEntity == nil {
		return nil, e
	}

	return fileEntity, nil
}

func (fileDAO *FileDAO) Delete(d *domain.File, tx access.Transacter) goerr.IError {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Add("DELETE FROM " + fileDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = fileDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	if _, e := queryer.Exec(queryBuilder.RawSQL(), queryBuilder.Values()...); e != nil {
		return e
	}

	return nil
}

func (fileDAO *FileDAO) Save(d *domain.File, tx access.Transacter) (
	*domain.File, goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	if d.ID != 0 {
		queryBuilder.Add("UPDATE " + fileDAO.Table())
		queryBuilder.Add(`SET  name = ? , roles = ? , created_at = ? , 
		user_id = ? , account_id = ? , extension = ? , link = ? , size = ?`,
			&d.Name, pq.Array(&d.Roles), &d.CreatedAt, &d.UserID, &d.AccountID, &d.Extension, &d.Link, &d.Size)
		queryBuilder.Add("WHERE id = ?", d.ID)
	} else {
		queryBuilder.Add("INSERT INTO " + fileDAO.Table())
		queryBuilder.Add(`( name , roles , created_at , 
			user_id , account_id , extension , link , size) VALUES ( ? , ? , ? , ? , ? , ? , ? , ?)`,
			&d.Name, pq.Array(&d.Roles), &d.CreatedAt, &d.UserID, &d.AccountID, &d.Extension, &d.Link, &d.Size)
	}

	queryBuilder.Add("RETURNING " + strings.Join(fileDAO.Columns(), ","))

	var queryer gopsql.Queryer
	queryer = fileDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	fileEntity, e := fileDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	return fileEntity, nil
}

func (fileDAO *FileDAO) Search(fileSearchForm *domain.FileSearchForm, tx access.Transacter) (
	files []*domain.File, pagination godict.Pagination, e goerr.IError) {
	queryBuilder := fileDAO.prepareSearchBuilder(fileSearchForm)

	countErrChan := make(chan utils.CounterErr, 1)

	queryer := fileDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	_, isTransaction := queryer.(*gopsql.Tx)

	if !isTransaction {
		utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	} else {
		go utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	}

	files, e = fileDAO.Collection(queryer.Query(queryBuilder.RawSQL(),
		queryBuilder.Values()...))
	if e != nil {
		return files, pagination, e
	}

	ch := <-countErrChan
	if ch.Err != nil {
		return files, pagination, ch.Err
	}

	pagination.Total = ch.Total

	return files, pagination, nil
}

func (fileDAO *FileDAO) prepareSearchBuilder(
	fileSearchForm *domain.FileSearchForm) *gopsql.Builder {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(fileDAO.Columns(), ","))
	queryBuilder.Add(" FROM " + fileDAO.Table() + " WHERE 1 = 1")

	if fileSearchForm.Query != "" {
		queryBuilder.Add("AND name LIKE ?",
			utils.ToLike(&fileSearchForm.Query))
	}

	if len(fileSearchForm.IDs) > 0 {
		queryBuilder.Add("AND id = ANY(?)",
			pq.Array(&fileSearchForm.IDs))
	}

	if len(fileSearchForm.ExcludedIDs) > 0 {
		queryBuilder.Add("AND NOT (id = ANY(?))",
			pq.Array(&fileSearchForm.ExcludedIDs))
	}

	queryBuilder.Pagination(fileSearchForm.Limit, fileSearchForm.Page-1)

	return queryBuilder
}

func (fileDAO *FileDAO) Collection(rows *sql.Rows, err goerr.IError) (collection []*domain.File,
	e goerr.IError) {
	collection = make([]*domain.File, 0)

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		e = err

		return
	}

	for rows.Next() {
		file := domain.File{}

		err := rows.Scan(fileDAO.Values(&file)...)
		if err != nil {
			e = goerr.New(err.Error())

			return
		}

		collection = append(collection, &file)
	}

	return collection, nil
}

func (fileDAO *FileDAO) One(rows *sql.Rows, err goerr.IError) (file *domain.File, e goerr.IError) {
	files, e := fileDAO.Collection(rows, err)
	if e != nil {
		return
	}

	if len(files) == 0 {
		return
	}

	return files[0], nil
}
