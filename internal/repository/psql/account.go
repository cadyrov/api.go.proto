package psql

import (
	"database/sql"
	"strings"

	"api.go.proto/internal/domain"
	"api.go.proto/internal/repository/access"
	"api.go.proto/utils"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
	"github.com/lib/pq"
)

type AccountDAO struct {
	gopsql.Queryer
}

func NewAccount(db gopsql.Queryer) *AccountDAO {
	return &AccountDAO{Queryer: db}
}

func (accountDAO *AccountDAO) Table() string {
	return "public.account"
}

func (accountDAO *AccountDAO) Columns() []string {
	return []string{"id", "name", "create_at", "deleted_at"}
}

func (accountDAO *AccountDAO) Values(d *domain.Account) (values []interface{}) {
	return append(values, &d.ID, &d.Name, &d.CreateAt, &d.DeletedAt)
}

func (accountDAO *AccountDAO) ByID(d *domain.Account, tx access.Transacter) (*domain.Account,
	goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(accountDAO.Columns(), ","))

	queryBuilder.Add("FROM " + accountDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = accountDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	accountEntity, e := accountDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	if accountEntity == nil {
		return nil, e
	}

	return accountEntity, nil
}

func (accountDAO *AccountDAO) Delete(d *domain.Account, tx access.Transacter) goerr.IError {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Add("DELETE FROM " + accountDAO.Table())

	queryBuilder.Add("WHERE id = ?", d.ID)

	var queryer gopsql.Queryer
	queryer = accountDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	if _, e := queryer.Exec(queryBuilder.RawSQL(), queryBuilder.Values()...); e != nil {
		return e
	}

	return nil
}

func (accountDAO *AccountDAO) Save(d *domain.Account, tx access.Transacter) (
	*domain.Account, goerr.IError) {
	queryBuilder := gopsql.NewBuilder()

	if d.ID != 0 {
		queryBuilder.Add("UPDATE " + accountDAO.Table())
		queryBuilder.Add("SET  name = ? , create_at = ? , deleted_at = ?", &d.Name, &d.CreateAt, &d.DeletedAt)
		queryBuilder.Add("WHERE id = ?", d.ID)
	} else {
		queryBuilder.Add("INSERT INTO " + accountDAO.Table())
		queryBuilder.Add("( name , create_at , deleted_at) VALUES ( ? , ? , ?)", &d.Name, &d.CreateAt, &d.DeletedAt)
	}

	queryBuilder.Add("RETURNING " + strings.Join(accountDAO.Columns(), ","))

	var queryer gopsql.Queryer
	queryer = accountDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	accountEntity, e := accountDAO.One(queryer.Query(queryBuilder.RawSQL(), queryBuilder.Values()...))
	if e != nil {
		return nil, e
	}

	return accountEntity, nil
}

func (accountDAO *AccountDAO) Search(accountSearchForm *domain.AccountSearchForm, tx access.Transacter) (
	accounts []*domain.Account, pagination godict.Pagination, e goerr.IError) {
	queryBuilder := accountDAO.prepareSearchBuilder(accountSearchForm)

	countErrChan := make(chan utils.CounterErr, 1)

	queryer := accountDAO.Queryer

	if tx != nil {
		queryer = tx.Querier()
	}

	_, isTransaction := queryer.(*gopsql.Tx)

	if !isTransaction {
		utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	} else {
		go utils.TotalFromQuery(countErrChan, *queryBuilder, queryer)
	}

	accounts, e = accountDAO.Collection(queryer.Query(queryBuilder.RawSQL(),
		queryBuilder.Values()...))
	if e != nil {
		return accounts, pagination, e
	}

	ch := <-countErrChan
	if ch.Err != nil {
		return accounts, pagination, ch.Err
	}

	pagination.Total = ch.Total

	return accounts, pagination, nil
}

func (accountDAO *AccountDAO) prepareSearchBuilder(
	accountSearchForm *domain.AccountSearchForm) *gopsql.Builder {
	queryBuilder := gopsql.NewBuilder()

	queryBuilder.Select(strings.Join(accountDAO.Columns(), ","))
	queryBuilder.Add(" FROM " + accountDAO.Table() + " WHERE 1 = 1")

	if accountSearchForm.Query != "" {
		queryBuilder.Add("AND name LIKE ?",
			utils.ToLike(&accountSearchForm.Query))
	}

	if len(accountSearchForm.IDs) > 0 {
		queryBuilder.Add("AND id = ANY(?)",
			pq.Array(&accountSearchForm.IDs))
	}

	if len(accountSearchForm.ExcludedIDs) > 0 {
		queryBuilder.Add("AND NOT (id = ANY(?))",
			pq.Array(&accountSearchForm.ExcludedIDs))
	}

	queryBuilder.Pagination(accountSearchForm.Limit, accountSearchForm.Page-1)

	return queryBuilder
}

func (accountDAO *AccountDAO) Collection(rows *sql.Rows, err goerr.IError) (collection []*domain.Account,
	e goerr.IError) {
	collection = make([]*domain.Account, 0)

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		e = err

		return
	}

	for rows.Next() {
		account := domain.Account{}

		err := rows.Scan(accountDAO.Values(&account)...)
		if err != nil {
			e = goerr.New(err.Error())

			return
		}

		collection = append(collection, &account)
	}

	return collection, nil
}

func (accountDAO *AccountDAO) One(rows *sql.Rows, err goerr.IError) (account *domain.Account, e goerr.IError) {
	accounts, e := accountDAO.Collection(rows, err)
	if e != nil {
		return
	}

	if len(accounts) == 0 {
		return
	}

	return accounts[0], nil
}
