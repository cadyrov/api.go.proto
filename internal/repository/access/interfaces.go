package access

import (
	"api.go.proto/internal/domain"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/cadyrov/gopsql"
)

type Transacter interface {
	Commit() goerr.IError
	Rollback() goerr.IError
	Querier() gopsql.Queryer
}

type AccountAI interface {
	Save(account *domain.Account, tx Transacter) (*domain.Account, goerr.IError)
	Delete(account *domain.Account, tx Transacter) goerr.IError
	ByID(account *domain.Account, tx Transacter) (*domain.Account, goerr.IError)
	Search(accountSearchForm *domain.AccountSearchForm, tx Transacter) ([]*domain.Account, godict.Pagination, goerr.IError)
}

type UserAI interface {
	Save(user *domain.User, tx Transacter) (*domain.User, goerr.IError)
	Delete(user *domain.User, tx Transacter) goerr.IError
	ByID(user *domain.User, tx Transacter) (*domain.User, goerr.IError)
	Search(userSearchForm *domain.UserSearchForm, tx Transacter) ([]*domain.User, godict.Pagination, goerr.IError)
	SearchIDs(userSearchForm *domain.UserSearchForm, tx Transacter) ([]*int, godict.Pagination, goerr.IError)
	SearchByAuthKey(activateSearchKeyForm string, tx Transacter) (*domain.User, goerr.IError)
}

type FileAI interface {
	Save(file *domain.File, tx Transacter) (*domain.File, goerr.IError)
	Delete(file *domain.File, tx Transacter) goerr.IError
	ByID(file *domain.File, tx Transacter) (*domain.File, goerr.IError)
	Search(fileSearchForm *domain.FileSearchForm, tx Transacter) ([]*domain.File, godict.Pagination, goerr.IError)
}
