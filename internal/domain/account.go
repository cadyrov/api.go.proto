package domain

import (
	"time"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
)

// Account domain.
type Account struct {
	AccountForm
	ID        int        `column:"id" json:"id"`
	CreateAt  time.Time  `column:"create_at" json:"createAt"`
	DeletedAt *time.Time `column:"deleted_at" json:"deletedAt"`
}

type AccountForm struct {
	Name string `column:"name" json:"name"`
}

type AccountRender struct {
	Account
}

type AccountSearchForm struct {
	SearchForm
}

func (m *Account) Render() *AccountRender {
	return &AccountRender{
		Account: Account{
			AccountForm: AccountForm{
				Name: m.Name,
			},
			ID:        m.ID,
			CreateAt:  m.CreateAt,
			DeletedAt: m.DeletedAt,
		},
	}
}

func (m *Account) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Name, validation.Required),
		validation.Field(&m.CreateAt, validation.Required),
	)
}
