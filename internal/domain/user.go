package domain

import (
	"fmt"
	"net/http"
	"time"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
	"github.com/cadyrov/govalidation/is"
)

const (
	AuthKeyLength = 32

	UserStateNew    = 10
	UserStateActive = 20
	UserStateBlock  = 30

	RoleRbacManage = "rbacmanage"
)

func Roles() map[string]struct{} {
	exists := make(map[string]struct{})

	exists[RoleRbacManage] = struct{}{}

	return exists
}

func IsRoles(roles ...string) (e goerr.IError) {
	exists := Roles()

	for i := range roles {
		if _, ok := exists[roles[i]]; ok {
			continue
		}

		if e == nil {
			e = goerr.New("validate error").HTTP(http.StatusBadRequest)
		}

		e.PushDetail(goerr.New(fmt.Sprintf("roles %s not exists", roles[i])).HTTP(http.StatusBadRequest))
	}

	return e
}

func (m *User) AddUserRole(roles ...string) {
	exists := make(map[string]struct{})
	for i := range m.Roles {
		exists[m.Roles[i]] = struct{}{}
	}

	for i := range roles {
		if _, ok := exists[roles[i]]; !ok {
			m.Roles = append(m.Roles, roles[i])
		}
	}
}

func (m *User) DropRole(roles ...string) {
	exists := make(map[string]struct{})
	for i := range m.Roles {
		exists[m.Roles[i]] = struct{}{}
	}

	for i := range roles {
		delete(exists, roles[i])
	}

	newRole := []string{}

	for key := range exists {
		newRole = append(newRole, key)
	}

	m.Roles = newRole
}

// User domain.
type User struct {
	UserForm
	ID                 int       `column:"id" json:"id"`
	AuthKey            string    `column:"auth_key" json:"authKey"`
	PasswordHash       string    `column:"password_hash" json:"passwordHash"`
	PasswordResetToken *string   `column:"password_reset_token" json:"passwordResetToken"`
	Email              string    `column:"email" json:"email"`
	AccountID          int       `column:"account_id" json:"accountId"`
	Status             int16     `column:"status" json:"status"`
	CreatedAt          time.Time `column:"created_at" json:"createdAt"`
	UpdatedAt          time.Time `column:"updated_at" json:"updatedAt"`
	Roles              []string  `column:"roles" json:"roles"`
	ProfileFileID      *int64    `column:"profile_file_id"`
}

type UserForm struct {
	Username   string  `column:"username" json:"username"`
	Surname    *string `column:"surname" json:"surname"`
	Middlename *string `column:"middlename" json:"middlename"`
	Phone      *string `column:"phone" json:"phone"`
	Name       *string `column:"name" json:"name"`
	Position   *string `column:"position" json:"position"`
}

type UserSearchForm struct {
	Email     string `json:"email"`
	AccountID int
	SearchForm
}

func (m *User) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Username, validation.Required),
		validation.Field(&m.AuthKey, validation.Required, validation.Length(AuthKeyLength, AuthKeyLength)),
		validation.Field(&m.PasswordHash, validation.Required),

		validation.Field(&m.Email, validation.Required, is.Email),
		validation.Field(&m.AccountID, validation.Required),
		validation.Field(&m.Status, validation.Required,
			validation.In(int16(UserStateNew), int16(UserStateActive), int16(UserStateBlock))),
		validation.Field(&m.CreatedAt, validation.Required),
		validation.Field(&m.UpdatedAt, validation.Required),
	)
}
