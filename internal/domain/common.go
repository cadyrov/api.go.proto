package domain

type SearchForm struct {
	Query       string  `json:"query"`
	IDs         []int64 `json:"ids"`
	ExcludedIDs []int64 `json:"excludedIds"`
	Limit       int     `json:"limit"`
	Page        int     `json:"page"`
}
