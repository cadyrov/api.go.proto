package domain

import (
	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
	"github.com/cadyrov/govalidation/is"
)

type RegistryForm struct {
	Email string `json:"email"`
}

type ActivateForm struct {
	Password string `json:"password"`
	Confirm  string `json:"confirm"`
	Key      string `json:"key"`
}

type ChangePasswordForm struct {
	Password string `json:"password"`
	Confirm  string `json:"confirm"`
}

type ActivateSearchKeyForm struct {
	Key string `json:"key"`
	SearchForm
}

type AuthorizationForm struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

const (
	minPasswordLenght = 6
	maxPasswordLenght = 40
)

func (m *RegistryForm) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Email, validation.Required, is.Email),
	)
}

func (m *ActivateForm) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Password, validation.Required, validation.Length(minPasswordLenght, maxPasswordLenght)),
		validation.Field(&m.Confirm, validation.Required, validation.In(m.Password)),
		validation.Field(&m.Key, validation.Required, validation.Length(AuthKeyLength, AuthKeyLength)),
	)
}

func (m *AuthorizationForm) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Login, validation.Required, is.Email),
		validation.Field(&m.Password, validation.Required),
	)
}

func (m *ChangePasswordForm) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Password, validation.Required, validation.Length(minPasswordLenght, maxPasswordLenght)),
		validation.Field(&m.Confirm, validation.Required, validation.In(m.Password)),
	)
}
