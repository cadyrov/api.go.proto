package domain

import (
	"time"

	"github.com/cadyrov/goerr"
	validation "github.com/cadyrov/govalidation"
)

// File domain.
type File struct {
	ID        int64     `column:"id" json:"id"`
	CreatedAt time.Time `column:"created_at" json:"createdAt"` // Дата создания
	UserID    int64     `column:"user_id" json:"userId"`       // ID пользователя
	AccountID int64     `column:"account_id" json:"accountId"` // ID аккаунта
	Extension string    `column:"extension" json:"extension"`  // расширение
	Link      string    `column:"link" json:"link"`            // ссылка
	Size      string    `column:"size" json:"size"`            // размер
	FileForm
}

type FileForm struct {
	Name  string   `column:"name" json:"name"`   // Название
	Roles []string `column:"roles" json:"roles"` // Роли

}

type FileRender struct {
	File
}

type FileSearchForm struct {
	SearchForm
}

func (m *File) Validate() (e goerr.IError) {
	return validation.ValidateStruct(m,
		validation.Field(&m.Name, validation.Required),
		validation.Field(&m.CreatedAt, validation.Required),
		validation.Field(&m.UserID, validation.Required),
		validation.Field(&m.AccountID, validation.Required),
		validation.Field(&m.Extension, validation.Required),
		validation.Field(&m.Link, validation.Required),
		validation.Field(&m.Size, validation.Required),
	)
}
