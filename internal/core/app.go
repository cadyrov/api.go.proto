package core

import (
	"net/http"
	"sync"

	"api.go.proto/internal/cache"
	"api.go.proto/internal/domain"
	"api.go.proto/internal/repository"
	"api.go.proto/tools/config"
	"api.go.proto/tools/dictionary"
	"api.go.proto/tools/i18n"
	"api.go.proto/tools/mailer"
	"api.go.proto/tools/mailgun"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/rs/zerolog/log"
)

type App struct {
	config     *config.Config
	repository *repository.Repository
	i18n       i18n.Translator
	dictionary *dictionary.Dictionary
	mailer     *mailer.Mailer
	mailgun    *mailgun.MailGun
	cache      cache.Cache
}

func New(config *config.Config, repository *repository.Repository,
	i18n i18n.Translator, dictionary *dictionary.Dictionary,
	mailer *mailer.Mailer, mailgun *mailgun.MailGun) *App {
	app := &App{
		config:     config,
		repository: repository,
		i18n:       i18n,
		dictionary: dictionary,
		mailer:     mailer,
		mailgun:    mailgun,
	}

	users, _, e := app.repository.UserAI.Search(&domain.UserSearchForm{}, nil)
	if e != nil {
		log.Fatal().Err(e)
	}

	accounts, _, e := app.repository.AccountAI.Search(&domain.AccountSearchForm{}, nil)
	if e != nil {
		log.Fatal().Err(e)
	}

	app.cache = cache.InitCache()

	var wg sync.WaitGroup

	for i := range users {
		wg.Add(1)

		go func(i int) {
			app.cache.UpdateUser(users[i])

			wg.Done()
		}(i)
	}

	for i := range accounts {
		wg.Add(1)

		go func(i int) {
			app.cache.UpdateAccount(accounts[i])

			wg.Done()
		}(i)
	}

	wg.Wait()

	return app
}

type Context struct {
	Locale     godict.Locale
	UserRender *UserRender
}

func (m *Context) HasOne(i18n i18n.Translator, roles ...string) goerr.IError {
	if m.UserRender == nil {
		return goerr.New(i18n.Translatef(m.Locale, "user_render_nil")).HTTP(http.StatusForbidden)
	}

	if !m.UserRender.HasOne(roles...) {
		return goerr.New(i18n.Translatef(m.Locale, "no_role")).HTTP(http.StatusForbidden)
	}

	return nil
}

const (
	UserStatusDictionary = "userStatus"
	MailSender           = "authorization@api.go.proto.online"
)

func (app *App) UserStatus(ctx Context, dictionaryID int) (
	render godict.DictionaryRender, e goerr.IError) {
	return app.dictionaryRender(ctx, UserStatusDictionary, dictionaryID)
}

func (app *App) dictionaryRender(ctx Context, dictionaryName string, dictionaryID int) (
	render godict.DictionaryRender, e goerr.IError) {
	dct, e := app.dictionary.ByLocale(ctx.Locale)
	if e != nil {
		return
	}

	return dct.DictionaryRender(dictionaryName, dictionaryID)
}
