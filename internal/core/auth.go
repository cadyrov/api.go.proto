package core

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"api.go.proto/internal/domain"
	"api.go.proto/tools/config"
	"api.go.proto/utils"
	"github.com/cadyrov/goerr"
	"github.com/dgrijalva/jwt-go"
)

var (
	ErrUnsupported        = goerr.New("some unsupported error")
	ErrDeadJWT            = errors.New("keys is dead")
	ErrNoConsistenceJWT   = errors.New("that's not even a tokenType")
	ErrIssuerNotAvailable = errors.New("issuer is not available in config")
	ErrParseJWT           = errors.New("can't parse claims")
	ErrKeyIsEmpty         = errors.New("please provide service tokenType, keys is empty")
)

func (app *App) Registry(ctx Context, registry *domain.RegistryForm) (e goerr.IError) {
	// validate
	registry.Email = strings.Trim(registry.Email, "")
	registry.Email = strings.ToLower(registry.Email)

	if e = registry.Validate(); e != nil {
		return
	}

	_, pgn, e := app.repository.UserAI.Search(&domain.UserSearchForm{Email: registry.Email}, nil)
	if e != nil {
		return
	}

	if pgn.Total > 0 {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_exists", registry.Email)).HTTP(http.StatusConflict)
	}

	acc := &domain.Account{
		AccountForm: domain.AccountForm{Name: registry.Email},
		CreateAt:    time.Now(),
	}
	if e := acc.Validate(); e != nil {
		return e
	}

	tx, e := app.repository.Begin()
	if e != nil {
		return
	}

	account, e := app.repository.AccountAI.Save(acc, tx)
	if e != nil {
		return
	}

	hash := utils.RandSeq(domain.AuthKeyLength)

	user := newUser(registry.Email, hash, account.ID)

	if e := user.Validate(); e != nil {
		return e
	}

	u, e := app.repository.UserAI.Save(user, tx)
	if e != nil {
		return
	}

	e = tx.Commit()
	if e != nil {
		return
	}

	app.cache.UpdateAccount(account)
	app.cache.UpdateUser(u)

	e = app.sendRegistryMail(ctx, registry.Email, hash)

	return e
}

func (app *App) sendRegistryMail(ctx Context, email string, hash string) goerr.IError {
	msg := app.mailgun.NewMessage(
		app.i18n.Translatef(ctx.Locale, "registry_mail_subject"),
		app.i18n.Translatef(ctx.Locale, "registry_mail_body",
			app.config.Project.URLRegistry, hash),
		email,
	)
	_, _, e := app.mailgun.Send(msg)

	return e
}

func newUser(email string, hash string, accountID int) *domain.User {
	return &domain.User{
		UserForm: domain.UserForm{
			Username: email,
		},
		AuthKey:      hash,
		PasswordHash: hash,
		Email:        email,
		AccountID:    accountID,
		Status:       int16(domain.UserStateNew),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		Roles:        []string{domain.RoleRbacManage},
	}
}

func (app *App) Activate(ctx Context, activate *domain.ActivateForm) (e goerr.IError) {
	if e = activate.Validate(); e != nil {
		return
	}

	user, e := app.repository.UserAI.SearchByAuthKey(activate.Key, nil)
	if user == nil {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found", nil)).HTTP(http.StatusNotFound)
	}

	if e != nil {
		return
	}

	if user.Status == domain.UserStateBlock {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_is_blocked", nil)).HTTP(http.StatusConflict)
	}

	hashPassword, e := utils.Hash(activate.Password)
	if e != nil {
		return
	}

	user.AuthKey = utils.RandSeq(domain.AuthKeyLength)
	user.PasswordHash = hashPassword
	user.Status = domain.UserStateActive

	if e = user.Validate(); e != nil {
		return
	}

	u, e := app.repository.UserAI.Save(user, nil)
	if e != nil {
		return
	}

	app.cache.UpdateUser(u)

	return e
}

func (app *App) RequestPassword(ctx Context, registry *domain.RegistryForm) (e goerr.IError) {
	registry.Email = strings.Trim(registry.Email, "")

	registry.Email = strings.ToLower(registry.Email)
	if e = registry.Validate(); e != nil {
		return
	}

	user := app.cache.FindUserByEmail(registry.Email)

	if user == nil {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found", nil)).HTTP(http.StatusNotFound)
	}

	if user.Status == domain.UserStateBlock {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_is_blocked", nil)).HTTP(http.StatusConflict)
	}

	user.AuthKey = utils.RandSeq(domain.AuthKeyLength)

	u, e := app.repository.UserAI.Save(user, nil)
	if e != nil {
		return
	}

	app.cache.UpdateUser(u)

	hash := user.AuthKey

	e = app.sendRequestPasswordMail(ctx, registry.Email, hash)

	return e
}

func (app *App) sendRequestPasswordMail(ctx Context, email string, hash string) goerr.IError {
	msg := app.mailgun.NewMessage(
		app.i18n.Translatef(ctx.Locale, "request_password_mail_subject"),
		app.i18n.Translatef(ctx.Locale, "request_password_mail_body",
			app.config.Project.URLRegistry, hash),
		email,
	)

	_, _, e := app.mailgun.Send(msg)

	return e
}

func (app *App) Authorization(ctx Context, auth *domain.AuthorizationForm) (jwtToken *string, e goerr.IError) {
	if e = auth.Validate(); e != nil {
		return
	}

	user := app.cache.FindUserByEmail(auth.Login)

	if user == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found")).HTTP(http.StatusNotFound)
	}

	if user.Status != domain.UserStateActive {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_active")).HTTP(http.StatusNotFound)
	}

	pass := user.PasswordHash
	userID := user.ID

	if e = utils.Compare(pass, auth.Password); e != nil {
		e = goerr.New(app.i18n.Translatef(ctx.Locale, "Login failed")).HTTP(http.StatusUnauthorized)

		return
	}

	jwtToken, err := CreateJWTToken(app.config.JWT, int64(userID))
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	return jwtToken, e
}

func (app *App) ValidateJwt(tokenString string) (sk *jwt.StandardClaims, e error) {
	if tokenString == "" {
		return nil, ErrKeyIsEmpty
	}

	sk = &jwt.StandardClaims{}

	token, err := jwt.ParseWithClaims(tokenString, sk,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(app.config.JWT.SecretKey), nil
		})
	if err != nil {
		return nil, err
	}

	if token.Valid {
		return sk, nil
	}

	if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return nil, ErrNoConsistenceJWT
		}

		if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			return nil, ErrDeadJWT
		}

		return nil, err
	}

	return nil, err
}

func CreateJWTToken(cnf config.JWT, userID int64) (*string, error) {
	claims := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * time.Duration(cnf.HourExpired)).Unix(),
		Issuer:    cnf.Issuer,
		Subject:   fmt.Sprintf("%d", userID),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(cnf.SecretKey))
	if err != nil {
		return nil, err
	}

	return &tokenString, nil
}

func (app *App) ChangePassword(ctx Context, changePass *domain.ChangePasswordForm) (e goerr.IError) {
	if e = changePass.Validate(); e != nil {
		return
	}

	hashPassword, e := utils.Hash(changePass.Password)
	if e != nil {
		return
	}

	userID := ctx.UserRender.ID
	user := app.cache.UserByID(&domain.User{ID: userID})
	user.AuthKey = utils.RandSeq(domain.AuthKeyLength)
	user.PasswordHash = hashPassword

	user.UpdatedAt = time.Now()
	if e = user.Validate(); e != nil {
		return
	}

	u, e := app.repository.UserAI.Save(user, nil)
	if e != nil {
		return
	}

	app.cache.UpdateUser(u)

	return e
}
