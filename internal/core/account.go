package core

import (
	"net/http"

	"api.go.proto/internal/domain"
	"github.com/cadyrov/goerr"
)

func (app *App) UpdateAccount(ctx Context, account *domain.Account) (
	*domain.AccountRender, goerr.IError) {
	oldData, e := app.repository.AccountAI.ByID(account, nil)
	if e != nil {
		return nil, e
	}

	oldData.Name = account.Name

	if e := oldData.Validate(); e != nil {
		return nil, e
	}

	if account.ID != ctx.UserRender.AccountID {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	accountEntity, e := app.repository.AccountAI.Save(oldData, nil)
	if e != nil {
		return nil, e
	}

	app.cache.UpdateAccount(accountEntity)

	accountRender := app.prepareAccountRender(ctx, *accountEntity)

	return accountRender, nil
}

func (app *App) GetAccount(ctx Context, account *domain.Account) (
	*domain.AccountRender, goerr.IError) {
	if account.ID != ctx.UserRender.AccountID {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	accountEntity := app.cache.AccountByID(account)

	if accountEntity == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "account_not_found")).HTTP(http.StatusNotFound)
	}

	if account.ID != ctx.UserRender.AccountID {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	accountRender := app.prepareAccountRender(ctx, *accountEntity)

	return accountRender, nil
}

func (app *App) prepareAccountRender(ctx Context, accountEntity domain.Account) *domain.AccountRender {
	_ = ctx

	return &domain.AccountRender{
		Account: accountEntity,
	}
}
