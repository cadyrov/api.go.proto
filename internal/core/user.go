package core

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"strings"
	"time"

	"api.go.proto/internal/domain"
	"api.go.proto/internal/repository/access"
	"api.go.proto/tools/filestorage"
	"api.go.proto/utils"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

type UserRender struct {
	ID         int                     `json:"id"`
	Email      string                  `json:"email"`
	AccountID  int                     `json:"accountId"`
	Status     godict.DictionaryRender `json:"status"`
	CreatedAt  time.Time               `json:"createdAt"`
	UpdatedAt  time.Time               `json:"updatedAt"`
	Roles      []string                `json:"roles"`
	Username   string                  `json:"username"`
	Surname    *string                 `json:"surname"`
	Middlename *string                 `json:"middlename"`
	Phone      *string                 `json:"phone"`
	Name       *string                 `json:"name"`
	Position   *string                 `json:"position"`
	ProfileImg *domain.FileRender      `json:"profileImg,omitempty"`
	Account    *domain.AccountRender   `json:"account,omitempty"`
}

func (app *App) CreateUser(ctx Context, registry *domain.RegistryForm) (e goerr.IError) {
	registry.Email = strings.Trim(registry.Email, "")

	registry.Email = strings.ToLower(registry.Email)
	if e = registry.Validate(); e != nil {
		return
	}

	_, pgn, e := app.repository.UserAI.Search(&domain.UserSearchForm{Email: registry.Email}, nil)
	if e != nil {
		return
	}

	if pgn.Total > 0 {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_exists", registry.Email)).HTTP(http.StatusConflict)
	}

	tx, e := app.repository.Begin()
	if e != nil {
		return
	}

	hash := utils.RandSeq(domain.AuthKeyLength)

	user := createUser(registry.Email, hash, ctx.UserRender.Account.ID)
	if e := user.Validate(); e != nil {
		return e
	}

	u, e := app.repository.UserAI.Save(user, tx)
	if e != nil {
		return
	}

	e = tx.Commit()
	if e != nil {
		return
	}

	app.cache.UpdateUser(u)

	e = app.sendRegistryMail(ctx, registry.Email, hash)
	if e != nil {
		return e
	}

	return e
}

func createUser(email string, hash string, accountID int) *domain.User {
	return &domain.User{
		UserForm: domain.UserForm{
			Username: email,
		},
		AuthKey:      hash,
		PasswordHash: hash,
		Email:        email,
		AccountID:    accountID,
		Status:       int16(domain.UserStateNew),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		Roles:        []string{},
	}
}

func (app *App) DeleteUser(ctx Context, user *domain.User) goerr.IError {
	us, e := app.repository.UserAI.ByID(user, nil)
	if e != nil {
		return e
	}

	if us.AccountID != ctx.UserRender.AccountID {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	if us.ID == ctx.UserRender.ID {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	if us.Status == domain.UserStateBlock {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_is_block")).HTTP(http.StatusConflict)
	}

	us.Status = domain.UserStateBlock
	us.UpdatedAt = time.Now()

	u, e := app.repository.UserAI.Save(us, nil)
	if e != nil {
		return e
	}

	app.cache.UpdateUser(u)

	return nil
}

func (app *App) UpdateUser(ctx Context, user *domain.User) (
	*UserRender, goerr.IError) {
	us, e := app.repository.UserAI.ByID(user, nil)
	if e != nil {
		return nil, e
	}

	if us.AccountID != ctx.UserRender.AccountID {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	if us.Status == domain.UserStateBlock {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_is_block")).HTTP(http.StatusConflict)
	}

	us.UserForm = user.UserForm
	us.UpdatedAt = time.Now()

	if e := us.Validate(); e != nil {
		return nil, e
	}

	userEntity, e := app.repository.UserAI.Save(us, nil)
	if e != nil {
		return nil, e
	}

	app.cache.UpdateUser(userEntity)

	var fr *domain.FileRender

	if userEntity.ProfileFileID != nil {
		fl, e := app.repository.FileAI.ByID(&domain.File{ID: *userEntity.ProfileFileID}, nil)
		if e != nil {
			return nil, e
		}

		fr = app.prepareFileRender(*fl)
	}

	userRender := app.prepareUserRender(ctx, *userEntity, nil, fr)

	return userRender, nil
}

func (app *App) GetUser(ctx Context, user *domain.User) (
	*UserRender, goerr.IError) {
	userEntity := app.cache.UserByID(user)

	if userEntity == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found")).HTTP(http.StatusNotFound)
	}

	userRender := app.prepareUserRender(ctx, *userEntity, nil, nil)

	return userRender, nil
}

func (app *App) SearchUser(ctx Context, searchUser *domain.UserSearchForm) (
	[]*UserRender, godict.Pagination, goerr.IError) {
	searchUser.AccountID = ctx.UserRender.Account.ID

	usersIDs, pagination, e := app.repository.UserAI.SearchIDs(searchUser, nil)
	if e != nil {
		return nil, pagination, e
	}

	renders := make([]*UserRender, 0, len(usersIDs))

	for i := range usersIDs {
		dm := &domain.User{ID: *usersIDs[i]}
		renders = append(renders, app.prepareUserRender(ctx, *app.cache.UserByID(dm), nil, nil))
	}

	return renders, pagination, nil
}

func (app *App) prepareUserRender(ctx Context, m domain.User,
	account *domain.AccountRender, profileImg *domain.FileRender) *UserRender {
	var statusRender godict.DictionaryRender

	if dct, e := app.dictionary.ByLocale(ctx.Locale); e == nil {
		if res, e := dct.DictionaryRender("userStatus", int(m.Status)); e == nil {
			statusRender = res
		}
	}

	r := UserRender{
		ID:         m.ID,
		Email:      m.Email,
		AccountID:  m.AccountID,
		Status:     statusRender,
		CreatedAt:  m.CreatedAt,
		UpdatedAt:  m.UpdatedAt,
		Roles:      m.Roles,
		Username:   m.Username,
		Surname:    m.Surname,
		Middlename: m.Middlename,
		Phone:      m.Phone,
		Name:       m.Name,
		Position:   m.Position,
		ProfileImg: profileImg,
		Account:    account,
	}

	return &r
}

func (app *App) UserByID(ctx Context, userID int) (*UserRender, goerr.IError) {
	u := app.cache.UserByID(&domain.User{ID: userID})
	if u == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_found")).HTTP(http.StatusNotFound)
	}

	if u.Status != domain.UserStateActive {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "user_not_active")).HTTP(http.StatusNotFound)
	}

	a := app.cache.AccountByID(&domain.Account{ID: u.AccountID})
	if a == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "account_not_found")).HTTP(http.StatusNotFound)
	}

	var fr *domain.FileRender

	if u.ProfileFileID != nil {
		fl, e := app.repository.FileAI.ByID(&domain.File{ID: *u.ProfileFileID}, nil)
		if e != nil {
			return nil, e
		}

		fr = app.prepareFileRender(*fl)
	}

	ur := app.prepareUserRender(ctx, *u, a.Render(), fr)

	return ur, nil
}

func (app *App) AddUserRole(ctx Context, userID int, role string) (e goerr.IError) {
	roles := strings.Split(strings.ToLower(role), ",")
	for i := range roles {
		roles[i] = strings.Trim(roles[i], " ")
	}

	if e = domain.IsRoles(roles...); e != nil {
		return e
	}

	us := &domain.User{ID: userID}

	user, e := app.repository.UserAI.ByID(us, nil)
	if e != nil {
		return e
	}

	if user.AccountID != ctx.UserRender.AccountID {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	user.AddUserRole(roles...)
	user.UpdatedAt = time.Now()

	u, e := app.repository.UserAI.Save(user, nil)
	if e != nil {
		return e
	}

	app.cache.UpdateUser(u)

	return e
}

func (app *App) DelUserRole(ctx Context, userID int, role string) (e goerr.IError) {
	roles := strings.Split(strings.ToLower(role), ",")

	for i := range roles {
		roles[i] = strings.Trim(roles[i], " ")
	}

	if e = domain.IsRoles(roles...); e != nil {
		return e
	}

	us := &domain.User{ID: userID}

	user, e := app.repository.UserAI.ByID(us, nil)
	if e != nil {
		return e
	}

	if user.AccountID != ctx.UserRender.AccountID {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	if user.ID == ctx.UserRender.ID {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	user.DropRole(roles...)
	user.UpdatedAt = time.Now()

	u, e := app.repository.UserAI.Save(user, nil)
	if e != nil {
		return e
	}

	app.cache.UpdateUser(u)

	return e
}

func (app *App) RestoreUser(ctx Context, user *domain.User) goerr.IError {
	us, e := app.repository.UserAI.ByID(user, nil)
	if e != nil {
		return e
	}

	if us.AccountID != ctx.UserRender.AccountID {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "access_forbidden")).HTTP(http.StatusForbidden)
	}

	if us.Status == domain.UserStateActive {
		return goerr.New(app.i18n.Translatef(ctx.Locale, "user_is_active")).HTTP(http.StatusConflict)
	}

	us.Status = domain.UserStateActive
	us.UpdatedAt = time.Now()

	u, e := app.repository.UserAI.Save(us, nil)
	if e != nil {
		return e
	}

	app.cache.UpdateUser(u)

	return nil
}

func (m *UserRender) HasOne(roles ...string) bool {
	exists := make(map[string]struct{})
	for i := range m.Roles {
		exists[m.Roles[i]] = struct{}{}
	}

	for _, n := range roles {
		if _, ok := exists[n]; ok {
			return true
		}
	}

	return false
}

const nameLength = 12

func (app *App) UploadImg(ctx *Context, files []*multipart.FileHeader) (fileRender *domain.FileRender, e goerr.IError) {
	if ctx.UserRender == nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "no_owner")).HTTP(http.StatusForbidden)
	}

	if len(files) == 0 {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "send_file")).HTTP(http.StatusBadRequest)
	}

	name, e := utils.RString(nameLength)
	if e != nil {
		return nil, e
	}

	newFl, e := app.saveFile(ctx, name, files[0], nil)
	if e != nil {
		return nil, e
	}

	user := app.cache.UserByID(&domain.User{ID: ctx.UserRender.ID})
	user.ProfileFileID = &newFl.ID

	_, e = app.repository.UserAI.Save(user, nil)
	if e != nil {
		return
	}

	app.cache.UpdateUser(user)

	return app.prepareFileRender(*newFl), nil
}

func (app *App) saveFile(ctx *Context, name string, file *multipart.FileHeader,
	tx access.Transacter) (newFl *domain.File, e goerr.IError) {
	if file == nil {
		return
	}

	runner := filestorage.NewS3Runner(app.config.S3)

	reader, err := file.Open()
	if err != nil {
		return nil, goerr.New(app.i18n.Translatef(ctx.Locale, "open_file"))
	}

	defer reader.Close()

	_, e = runner.AWSUpload(reader, name)
	if e != nil {
		return
	}

	nm, ext := utils.ParseExtension(file.Filename)

	fl := &domain.File{}
	fl.AccountID = int64(ctx.UserRender.AccountID)
	fl.UserID = int64(ctx.UserRender.ID)
	fl.CreatedAt = time.Now()
	fl.Name = nm
	fl.Extension = ext
	fl.Link = name
	fl.Size = fmt.Sprintf("%d", file.Size)
	fl.Roles = []string{}

	if e = fl.Validate(); e != nil {
		return
	}

	newFl, e = app.repository.FileAI.Save(fl, tx)
	if e != nil {
		return
	}

	return newFl, e
}

func (app *App) prepareFileRender(df domain.File) *domain.FileRender {
	fr := &domain.FileRender{
		File: df,
	}

	fr.Link = app.config.S3.Endpoint + "/" + app.config.S3.Bucket + "/" + df.Link

	return fr
}
