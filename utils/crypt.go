package utils

import (
	"net/http"

	"github.com/cadyrov/goerr"
	"golang.org/x/crypto/bcrypt"
)

func Hash(data string) (hash string, e goerr.IError) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(data), bcrypt.DefaultCost)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	return string(hashed), e
}

func Compare(data string, aim string) goerr.IError {
	err := bcrypt.CompareHashAndPassword([]byte(data), []byte(aim))
	if err != nil {
		return goerr.New(err.Error()).HTTP(http.StatusBadRequest)
	}

	return nil
}
