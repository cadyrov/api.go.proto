package utils

import (
	"encoding/base64"
	"math/rand"
	"strings"

	"github.com/cadyrov/goerr"
)

func StringPtr(in string) *string {
	return &in
}

//nolint
func RString(length int) (string, goerr.IError) {
	b := make([]byte, length)

	_, err := rand.Read(b)
	if err != nil {
		return "", goerr.New(err.Error())
	}

	return base64.URLEncoding.EncodeToString(b), nil
}

// nolint
func RandSeq(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-")

	b := make([]rune, n)

	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}

func ParseExtension(fName string) (string, string) {
	parts := strings.Split(fName, ".")

	ln := len(parts)

	minLen := 2

	if ln < minLen {
		return fName, ""
	}

	return strings.Join(parts[:ln-1], "."), parts[ln-1]
}
