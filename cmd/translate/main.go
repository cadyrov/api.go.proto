package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"

	"api.go.proto/tools/config"
	"api.go.proto/utils"
	"github.com/cadyrov/godict"
	"github.com/jessevdk/go-flags"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

const defaultConcurrency = 10

func main() {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	concurrency := flag.Int("concurrency", defaultConcurrency, "count of routines to prepare files")

	flag.Parse()

	rootPath := utils.RootPath()

	conf := &config.Config{}

	parser := flags.NewParser(conf, flags.Default)
	if _, err := parser.Parse(); err != nil {
		fmt.Printf("error parse env: %s\n", err.Error())
		os.Exit(1)
	}

	fileNames := getGoFiles(rootPath, []string{"vendor", "cmd"})

	result := goFiles(fileNames, *concurrency)

	vocabular := make(map[string]string)

	ymlpth := rootPath + string(os.PathSeparator) + conf.Project.I18nPath +
		string(os.PathSeparator) + strconv.Itoa(int(conf.Project.DefaultLocale)) +
		string(os.PathSeparator) + "data.yml"

	bt, err := ioutil.ReadFile(ymlpth)
	if err != nil {
		log.Err(err)

		return
	}

	if e := godict.YamlUnmarshal(bt, &vocabular); e != nil {
		return
	}

	for i := range result {
		if _, ok := vocabular[result[i]]; !ok {
			txt := strings.Join(strings.Split(result[i], "_"), " ")

			r := []rune(txt)

			vocabular[result[i]] = strings.ToUpper(string(r[0])) + string(r[1:])
		}
	}

	writeYaml(ymlpth, vocabular)
}

func writeYaml(path string, vocabular map[string]string) {
	out, err := godict.YamlMarshal(vocabular)
	if err != nil {
		log.Err(err)

		return
	}

	if err := ioutil.WriteFile(path, out, os.ModePerm); err != nil {
		log.Err(err)

		return
	}
}

func goFiles(fileNames []string, routines int) []string {
	result := []string{}
	resChan := make(chan []string)
	chCount := make(chan int, routines)

	for i := range fileNames {
		chCount <- 1

		go parseFileTotemplate(fileNames[i], resChan, chCount)
	}

	for i := 0; i < len(fileNames); i++ {
		res := <-resChan

		result = append(result, res...)
	}

	return result
}

func parseFileTotemplate(path string, resChan chan []string, chCount chan int) {
	res := make([]string, 0)

	var err error

	defer func() {
		if err != nil {
			log.Err(err)
		}

		<-chCount

		resChan <- res
	}()

	log.Info().Str("start parse", path)

	fl, err := os.Open(path)
	if err != nil {
		return
	}

	if fl == nil {
		return
	}

	defer fl.Close()

	bt, err := ioutil.ReadAll(fl)
	if err != nil {
		return
	}

	re := regexp.MustCompile(`Translatef\(ctx.Locale, ".*"`)

	res = re.FindAllString(string(bt), -1)

	for i := range res {
		res[i] = strings.ReplaceAll(res[i], `Translatef(ctx.Locale, "`, "")
		res[i] = strings.ReplaceAll(res[i], `"`, "")
	}

	log.Info().Int("stop parse found cases", len(res))
}

func getGoFiles(path string, excludedDir []string) (res []string) {
	fls, err := ioutil.ReadDir(path)
	if err != nil {
		log.Err(err)

		return
	}

	for i := range fls {
		if fls[i].IsDir() {
			var excludExist bool

			for j := range excludedDir {
				if excludedDir[j] == fls[i].Name() {
					excludExist = true
				}
			}

			if excludExist {
				continue
			}

			res = append(res, getGoFiles(path+string(os.PathSeparator)+fls[i].Name(),
				excludedDir)...)
		}

		if !fls[i].IsDir() {
			if strings.Contains(fls[i].Name(), ".go") {
				res = append(res, path+string(os.PathSeparator)+fls[i].Name())
			}
		}
	}

	return res
}
