package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"time"

	"api.go.proto/api/router"
	config "api.go.proto/tools/config"
	"github.com/jessevdk/go-flags"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	cnf, srv, app := initServer()

	go func() {
		if cnf.Web.SSLSertPath != "" && cnf.Web.SSLKeyPath != "" {
			if err := srv.ListenAndServeTLS(cnf.Web.SSLSertPath, cnf.Web.SSLKeyPath); err != nil {
				log.Fatal().Err(err)

				panic(err)
			}

			return
		}

		if err := srv.ListenAndServe(); err != nil {
			log.Fatal().Err(err)

			panic(err)
		}
	}()

	log.Warn().
		Str("host", app.Config.Web.Host).
		Int("port", app.Config.Web.Port).
		Msg("server start")

	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt)

	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(app.Config.ReadTimeout))

	if err := srv.Shutdown(ctx); err != nil {
		cancel()

		panic(err)
	}

	log.Warn().Msg("server stop")

	cancel()

	runtime.Goexit()
}

func initServer() (*config.Config, *http.Server, *router.App) {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	conf := &config.Config{}

	parser := flags.NewParser(conf, flags.Default)
	if _, err := parser.Parse(); err != nil {
		fmt.Printf("error parse env: %s\n", err.Error())
		os.Exit(1)
	}

	app := router.New(conf)

	srv := &http.Server{
		Handler:      app.GetRoutes(),
		Addr:         app.Config.Web.Host + ":" + strconv.Itoa(app.Config.Web.Port),
		ReadTimeout:  time.Second * time.Duration(app.Config.Web.ReadTimeout),
		WriteTimeout: time.Second * time.Duration(app.Config.Web.WriteTimeout),
		IdleTimeout:  time.Second * time.Duration(app.Config.Web.IdleTimeout),
	}

	return conf, srv, app
}
