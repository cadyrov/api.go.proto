package main

import (
	"fmt"
	"os"

	"api.go.proto/internal/repository"
	"api.go.proto/tools/config"
	"api.go.proto/utils"
	"github.com/cadyrov/gopsql"
	"github.com/jessevdk/go-flags"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Psr struct {
	config.Config
	Domain     bool   `long:"domain"`
	PSQL       bool   `long:"psql"`
	Core       bool   `long:"core"`
	Controller bool   `long:"controller"`
	Route      bool   `long:"route"`
	Tests      bool   `long:"tests"`
	Name       string `long:"name"`
}

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	rootPath := utils.RootPath()

	psr := &Psr{}

	parser := flags.NewParser(psr, flags.Default)
	if _, err := parser.Parse(); err != nil {
		fmt.Printf("error parse env: %s\n", err.Error())
		os.Exit(1)
	}

	log.Error().Interface("settings", psr)

	if psr.Name == "" {
		log.Error().Msg("set name")

		return
	}

	repo, e := repository.New(repository.NewConfig(psr.Config.PSQL), false)
	if e != nil {
		panic(e)
	}

	createDomain(psr.Name, repo, rootPath, psr.Config, psr.Domain)
	createPSQL(psr.Name, repo, rootPath, psr.Config, psr.PSQL)
	createCore(psr.Name, repo, rootPath, psr.Config, psr.Core)
	createController(psr.Name, repo, rootPath, psr.Config, psr.Controller)
	createRoute(psr.Name, repo, rootPath, psr.Config, psr.Route)
	createTests(psr.Name, repo, rootPath, psr.Config, psr.Tests)
}

func createDomain(name string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.DomainPath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.DomainTemplatePath)
	if err != nil {
		log.Err(err)

		return
	}

	log.Print("success create new domain", name)
}

func createPSQL(name string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.PSQLPath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.PSQLTemplatePath)
	if err != nil {
		log.Err(err)

		return
	}

	log.Print("success create new psql", name)
}

func createCore(name string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.CorePath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.CoreTemplatePath)
	if err != nil {
		log.Err(err)

		return
	}

	log.Info().Str("success create new core: ", name)
}

func createController(name string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.ControllerPath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.ControllerTemplatePath)
	if err != nil {
		log.Err(err)

		return
	}

	log.Info().Str("success create new controller", name)
}

func createRoute(name string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	if !exec {
		return
	}

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.RoutePath+"/"+name+".go",
		"public", name, rootPath+"/"+config.Generator.RouteTemplatePath)
	if err != nil {
		log.Err(err)

		return
	}

	log.Info().Str("success create new route: ", name)
}

func createTests(tableName string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	createTestEntities(tableName, repo, rootPath, config, exec)
	createCoreTest(tableName, repo, rootPath, config, exec)
}

func createTestEntities(name string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	if !exec {
		return
	}

	fileName := name + "_entities"

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.TestEntitiesPath+"/"+fileName+".go",
		"public", name, rootPath+"/"+config.Generator.TestEntitiesTemplatePath)
	if err != nil {
		log.Err(err)

		return
	}

	log.Info().Str("success create new test entities: ", fileName)
}

func createCoreTest(name string, repo *repository.Repository, rootPath string,
	config config.Config, exec bool) {
	if !exec {
		return
	}

	fileName := name + "_test"

	err := gopsql.MakeModel(repo.PSQLQueryer,
		rootPath+"/"+config.Generator.CoreTestsPath+"/"+fileName+".go",
		"public", name, rootPath+"/"+config.Generator.CoreTestsTemplatePath)
	if err != nil {
		log.Err(err)

		return
	}

	log.Info().Str("success create new core tests: ", fileName)
}
