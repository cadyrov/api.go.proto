package controller

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"runtime/debug"
	"strconv"
	"strings"

	"api.go.proto/internal/core"
	"api.go.proto/internal/domain"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/getsentry/sentry-go"
)

type contextKey string

const contextKeyName contextKey = "localecontext"

var (
	ErrUnsupported        = goerr.New("some unsupported error")
	ErrDeadJWT            = errors.New("keys is dead")
	ErrNoConsistenceJWT   = errors.New("that's not even a tokenType")
	ErrIssuerNotAvailable = errors.New("issuer is not available in config")
	ErrParseJWT           = errors.New("can't parse claims")
	ErrKeyIsEmpty         = errors.New("please provide service tokenType, keys is empty")
)

func (app *App) ContextFromRequest(request *http.Request) *core.Context {
	requestContext := request.Context().Value(contextKeyName)

	if requestContext == nil {
		return &core.Context{Locale: godict.ENLocale}
	}

	coreContext := requestContext.(core.Context)

	return &coreContext
}

func (app *App) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cook, err := r.Cookie(app.Config.Web.CookieName)
		if err != nil {
			e := goerr.New("Check authorization error:" + err.Error()).HTTP(http.StatusUnauthorized)
			godict.SendError(w, e)

			return
		}
		if cook == nil {
			e := goerr.New("Check authorization error:" + err.Error()).HTTP(http.StatusUnauthorized)
			godict.SendError(w, e)

			return
		}

		sk, err := app.Core.ValidateJwt(cook.Value)
		if err != nil {
			e := goerr.New("Check authorization error:" + err.Error()).HTTP(http.StatusUnauthorized)

			godict.SendError(w, e)

			return
		}
		if sk == nil {
			e := goerr.New("Check authorization error:" + err.Error()).HTTP(http.StatusUnauthorized)

			godict.SendError(w, e)

			return
		}

		ctx := app.ContextFromRequest(r)

		sb := sk.Subject
		idUser, err := strconv.Atoi(sb)
		if err != nil {
			godict.SendError(w, goerr.New(err.Error()).HTTP(http.StatusUnauthorized))

			return
		}

		renderUser, e := app.Core.UserByID(*ctx, idUser)
		if e != nil {
			godict.SendError(w, e)

			return
		}

		if renderUser == nil && renderUser.Status.ID != domain.UserStateActive {
			godict.SendError(w, goerr.New(err.Error()).HTTP(http.StatusUnauthorized))

			return
		}

		cn := core.Context{Locale: godict.ENLocale, UserRender: renderUser}

		httpcontext := context.WithValue(r.Context(), contextKeyName, cn)

		next.ServeHTTP(w, r.WithContext(httpcontext))
	})
}

func corsHeaders(responseWriter http.ResponseWriter, corsHeader string) {
	responseWriter.Header().Set("Access-Control-Allow-Origin", corsHeader)
	responseWriter.Header().Set("Access-Control-Allow-Credentials", "true")
	responseWriter.Header().Set("Access-Control-Allow-Methods", "GET,POST,PATCH,PUT,UPDATE,DELETE,OPTIONS")
	responseWriter.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Key, Authorization, Cookie")
}

func (app *App) CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
		origin := checkCorsHeaders(app, request)

		corsHeaders(responseWriter, origin)

		next.ServeHTTP(responseWriter, request)
	})
}

func checkCorsHeaders(app *App, request *http.Request) string {
	corses := strings.Split(app.Config.Cors, ",")

	origin := request.Header.Get("Origin")

	var corsHeader string

	for i := range corses {
		if strings.Contains(corses[i], origin) {
			corsHeader = corses[i]

			return corsHeader
		}
	}

	return corsHeader
}

// Метод не доступен.
func (app *App) NotAllowedMiddleware(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		origin := checkCorsHeaders(app, r)

		corsHeaders(w, origin)

		return
	}

	e := goerr.New("requested method is not allowed").HTTP(http.StatusMethodNotAllowed)

	godict.Send(w, godict.Error(e))
}

func (app *App) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				var e goerr.IError
				switch req := r.(type) {
				case goerr.IError:
					e = req
				case error:
					e = goerr.New(req.Error())
				case string:
					e = goerr.New(req)
				default:
					e = ErrUnsupported
				}
				key := "stack"
				message := fmt.Sprintf("%s\n%s", e.Error(), debug.Stack())
				e.PushDetail(goerr.New(key + "callback" + message))
				log.Println(e)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func (app *App) GetContext(request *http.Request) (ctx core.Context, e goerr.IError) {
	val := request.Context().Value(contextKeyName)
	if val == nil {
		e = goerr.New("user in context not found").HTTP(http.StatusNotFound)

		return
	}

	ctx = val.(core.Context)

	return
}

func sendError(w http.ResponseWriter, e goerr.IError) {
	if e.GetCode() != http.StatusBadRequest &&
		e.GetCode() != http.StatusUnauthorized {
		sentry.CaptureException(e)
	}

	godict.SendError(w, e)
}
