package controller

import (
	"net/http"
	"time"

	"api.go.proto/internal/domain"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

func (app *App) Registry(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	form := &domain.RegistryForm{}

	if e := godict.ParseBody(r.Body, form); e != nil {
		godict.SendError(w, e)

		return
	}

	if e := app.Core.Registry(*ctx, form); e != nil {
		godict.SendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "registry_success"), nil, nil))
}

func (app *App) Activate(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	activateForm := &domain.ActivateForm{}

	parseError := godict.ParseBody(r.Body, &activateForm)
	if parseError != nil {
		sendError(w, parseError)

		return
	}

	e := app.Core.Activate(*ctx, activateForm)
	if e != nil {
		godict.SendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "user_activated"), nil, nil))
}

func (app *App) RequestPassword(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	form := &domain.RegistryForm{}

	if e := godict.ParseBody(r.Body, form); e != nil {
		godict.SendError(w, e)

		return
	}

	if e := app.Core.RequestPassword(*ctx, form); e != nil {
		godict.SendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "request_password_success"), nil, nil))
}

func (app *App) Authorization(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	form := &domain.AuthorizationForm{}

	if e := godict.ParseBody(r.Body, form); e != nil {
		godict.SendError(w, e)

		return
	}

	jwtToken, e := app.Core.Authorization(*ctx, form)
	if e != nil {
		godict.SendError(w, e)

		return
	}

	if jwtToken == nil {
		godict.SendError(w,
			goerr.New(app.I18n.Translatef(ctx.Locale,
				"coudn't create JWT token")).HTTP(http.StatusUnauthorized))

		return
	}

	// write the cookie to response
	ck := &http.Cookie{
		Name:    app.Config.Web.CookieName,
		Domain:  "",
		Path:    "",
		Expires: time.Now().Add(time.Hour * time.Duration(app.Config.JWT.HourExpired)),
		Value:   *jwtToken,
	}

	if app.Config.Web.SameSiteNone == 1 {
		ck.SameSite = http.SameSiteNoneMode
		ck.Secure = true
	}

	http.SetCookie(w, ck)

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "authorization_success"), nil, nil))
}

func (app *App) Check(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "success"), ctx.UserRender, nil))
}

func (app *App) Logout(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	ck := &http.Cookie{
		Name:    app.Config.Web.CookieName,
		Domain:  "",
		Path:    "",
		Expires: time.Now().Add(time.Hour * time.Duration(-app.Config.JWT.HourExpired)),
		Value:   "",
	}

	if app.Config.Web.SameSiteNone == 1 {
		ck.SameSite = http.SameSiteNoneMode
		ck.Secure = true
	}

	http.SetCookie(w, ck)

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "success"), nil, nil))
}

func (app *App) ChangePassword(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	changePass := &domain.ChangePasswordForm{}

	parseError := godict.ParseBody(r.Body, &changePass)
	if parseError != nil {
		sendError(w, parseError)

		return
	}

	e := app.Core.ChangePassword(*ctx, changePass)
	if e != nil {
		godict.SendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "user_activated"), nil, nil))
}
