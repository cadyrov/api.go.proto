package controller

import (
	"net/http"
	"strconv"

	"api.go.proto/internal/domain"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
)

func (app *App) UpdateAccount(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	accountID, parseIntError := strconv.Atoi(mux.Vars(r)["id"])
	if parseIntError != nil {
		sendError(w, goerr.New(parseIntError.Error()))

		return
	}

	form := domain.AccountForm{}

	parseBodyError := godict.ParseBody(r.Body, &form)
	if parseBodyError != nil {
		godict.Send(w, godict.Error(parseBodyError))

		return
	}

	accountEntity := domain.Account{ID: accountID, AccountForm: form}

	account, e := app.Core.UpdateAccount(*ctx, &accountEntity)
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "update_account"), account, nil))
}

func (app *App) GetAccount(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	accountID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	account, e := app.Core.GetAccount(*ctx, &domain.Account{ID: accountID})
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "get_account"),
		account, nil))
}
