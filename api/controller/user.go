package controller

import (
	"net/http"
	"strconv"

	"api.go.proto/internal/domain"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/gorilla/mux"
)

func (app *App) CreateUser(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	form := &domain.RegistryForm{}
	if e := godict.ParseBody(r.Body, form); e != nil {
		godict.SendError(w, e)

		return
	}

	if e := app.Core.CreateUser(*ctx, form); e != nil {
		godict.SendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "create_user"), nil, nil))
}

func (app *App) UpdateUser(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	userID, parseIntError := strconv.Atoi(mux.Vars(r)["id"])
	if parseIntError != nil {
		sendError(w, goerr.New(parseIntError.Error()))

		return
	}

	form := domain.UserForm{}

	parseBodyError := godict.ParseBody(r.Body, &form)
	if parseBodyError != nil {
		godict.Send(w, godict.Error(parseBodyError))

		return
	}

	userEntity := domain.User{ID: userID, UserForm: form}

	user, e := app.Core.UpdateUser(*ctx, &userEntity)
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "update_user"), user, nil))
}

func (app *App) DeleteUser(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	userID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	userEntity := domain.User{ID: userID}

	e := app.Core.DeleteUser(*ctx, &userEntity)
	if e != nil {
		godict.Send(w, godict.Error(goerr.New(e.Error())))

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "delete_user"), nil, nil))
}

func (app *App) GetUser(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	userID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	user, e := app.Core.GetUser(*ctx, &domain.User{ID: userID})
	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "get_user"),
		user, nil))
}

func (app *App) SearchUser(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)

	userSearchForm := &domain.UserSearchForm{}

	parseError := godict.ParseBody(r.Body, &userSearchForm)
	if parseError != nil {
		godict.Send(w, godict.Error(parseError))

		return
	}

	user, pagination, e := app.Core.SearchUser(*ctx, userSearchForm)
	if e != nil {
		godict.SendError(w, goerr.New(e.Error()).HTTP(http.StatusNotFound))

		return
	}

	godict.SendOk(w, app.I18n.Translatef(ctx.Locale, "search_user"),
		user, pagination)
}

func (app *App) AddUserRole(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	userID, parseIntError := strconv.Atoi(mux.Vars(r)["id"])
	if parseIntError != nil {
		sendError(w, goerr.New(parseIntError.Error()))

		return
	}

	roles := r.URL.Query().Get("role")

	e := app.Core.AddUserRole(*ctx, userID, roles)

	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "add_role_success"), nil, nil))
}

func (app *App) DelUserRole(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	userID, parseIntError := strconv.Atoi(mux.Vars(r)["id"])
	if parseIntError != nil {
		sendError(w, goerr.New(parseIntError.Error()))

		return
	}

	roles := r.URL.Query().Get("role")

	e := app.Core.DelUserRole(*ctx, userID, roles)

	if e != nil {
		sendError(w, e)

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "delete_role_success"), nil, nil))
}

func (app *App) RestoreUser(w http.ResponseWriter, r *http.Request) {
	ctx := app.ContextFromRequest(r)
	if e := ctx.HasOne(app.I18n, domain.RoleRbacManage); e != nil {
		sendError(w, e)

		return
	}

	userID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		sendError(w, goerr.New(err.Error()))

		return
	}

	userEntity := domain.User{ID: userID}

	e := app.Core.RestoreUser(*ctx, &userEntity)
	if e != nil {
		godict.Send(w, godict.Error(goerr.New(e.Error())))

		return
	}

	godict.Send(w, godict.Ok(app.I18n.Translatef(ctx.Locale, "user_active"), nil, nil))
}

func (app *App) UploadImg(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(0)
	if err != nil {
		godict.SendError(w, goerr.New("Form parse error: "+err.Error()).HTTP(http.StatusBadRequest))

		return
	}

	ctx := app.ContextFromRequest(r)

	fileRender, e := app.Core.UploadImg(ctx, r.MultipartForm.File["file"])
	if e != nil {
		godict.SendError(w, e)

		return
	}

	godict.SendOk(w, app.I18n.Translatef(ctx.Locale, "file_create"), fileRender, nil)
}
