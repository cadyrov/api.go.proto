package controller

import (
	"api.go.proto/internal/core"
	"api.go.proto/tools/config"
	"api.go.proto/tools/dictionary"
	"api.go.proto/tools/i18n"
)

type App struct {
	Config     *config.Config
	I18n       i18n.Translator
	Dictionary *dictionary.Dictionary
	Core       *core.App
}
