package router

import (
	"net/http"

	"api.go.proto/internal/domain"
	"github.com/gorilla/mux"
)

// RegistryForm
//
// swagger:parameters RegistryForm RequestPasswordForm
type RegistryForm struct {
	// in: body
	// required: true
	Body struct {
		domain.RegistryForm
	}
}

// ActivateUserForm
//
// swagger:parameters ActivateUserForm
type ActivateUserForm struct {
	// in: body
	// required: true
	Body struct {
		domain.ActivateForm
	}
}

// AuthorizationForm
//
// swagger:parameters AuthorizationForm
type AuthorizationForm struct {
	// in: body
	// required: true
	Body struct {
		domain.AuthorizationForm
	}
}

func (app *App) SetAuthRoutes(router *mux.Router) {
	registry(app, router)
	activate(app, router)
	requestPassword(app, router)
	authorization(app, router)
}

func registry(app *App, router *mux.Router) {
	// swagger:route POST /v1/auth/registry  Auth RegistryForm
	//
	// Registry
	//
	// Registry new user and account and send email with verification
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/registry", app.Registry).Methods(http.MethodPost)
}

func activate(app *App, router *mux.Router) {
	// swagger:route POST /v1/auth/activate  Auth ActivateUserForm
	//
	// Activate
	//
	// Activate user
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/activate", app.Activate).Methods(http.MethodPost)
}

func requestPassword(app *App, router *mux.Router) {
	// swagger:route POST /v1/auth/requestpassword  Auth RequestPasswordForm
	//
	// Request to change password
	//
	// Request to change password for not authorized user and send email with verification
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       404: ResponseError
	//       409: ResponseError
	//       500: ResponseError
	router.HandleFunc("/requestpassword", app.RequestPassword).Methods(http.MethodPost)
}

func authorization(app *App, router *mux.Router) {
	// swagger:route POST /v1/auth  Auth AuthorizationForm
	//
	// Login
	//
	// Service authorization
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.Authorization).Methods(http.MethodPost)
}
