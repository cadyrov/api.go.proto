package router

import (
	"net/http"

	"api.go.proto/api/controller"
	"api.go.proto/internal/core"
	"api.go.proto/internal/domain"
	"api.go.proto/internal/repository"
	"api.go.proto/tools/config"
	"api.go.proto/tools/dictionary"
	"api.go.proto/tools/i18n"
	"api.go.proto/tools/mailgun"
	"github.com/gorilla/mux"
)

type App struct {
	controller.App
}

func New(c *config.Config) *App {
	translator, e := i18n.New(c.Project.DefaultLocale, c)
	if e != nil {
		panic(e)
	}

	dict, e := dictionary.New(c.Project.DefaultLocale, c)
	if e != nil {
		panic(e)
	}

	repo, e := repository.New(repository.NewConfig(c.PSQL), false)
	if e != nil {
		panic(e)
	}

	mailgun := mailgun.NewMailGunService(c.Mailgun)

	cr := core.New(c, repo, translator, dict, nil, mailgun)

	return &App{
		controller.App{
			Config:     c,
			I18n:       translator,
			Dictionary: dict,
			Core:       cr,
		},
	}
}

func (app *App) GetRoutes() *mux.Router {
	routes := mux.NewRouter()
	routes.Use(app.LoggingMiddleware)
	routes.Use(app.CorsMiddleware)
	routes.MethodNotAllowedHandler = http.HandlerFunc(app.NotAllowedMiddleware)

	mainRoute := routes.PathPrefix("/api").Subrouter()

	system := mainRoute.PathPrefix("/system").Subrouter()
	app.SetSystemRoutes(system)

	dictionaryRoute := mainRoute.PathPrefix("/dictionaries").Subrouter()
	app.SetDictionariesRoutes(dictionaryRoute)

	v1Route := mainRoute.PathPrefix("/v1").Subrouter()

	accountRoute := v1Route.PathPrefix("/account").Subrouter()
	app.SetAccountRoutes(accountRoute)
	accountRoute.Use(app.AuthMiddleware)

	userRoute := v1Route.PathPrefix("/user").Subrouter()
	app.SetUserRoutes(userRoute)
	userRoute.Use(app.AuthMiddleware)

	authRoute := v1Route.PathPrefix("/auth").Subrouter()
	app.SetAuthRoutes(authRoute)

	checkRoute := v1Route.PathPrefix("/check").Subrouter()
	check(app, checkRoute)
	checkRoute.Use(app.AuthMiddleware)

	logoutRoute := v1Route.PathPrefix("/logout").Subrouter()
	logout(app, logoutRoute)
	logoutRoute.Use(app.AuthMiddleware)

	changePasswordRoute := v1Route.PathPrefix("/changepassword").Subrouter()
	changePassword(app, changePasswordRoute)
	changePasswordRoute.Use(app.AuthMiddleware)

	return routes
}

// ChangePasswordForm
//
// swagger:parameters  ChangePasswordForm
type ChangePasswordForm struct {
	// in: body
	// required: true
	Body struct {
		domain.ChangePasswordForm
	}
}

func check(app *App, router *mux.Router) {
	// swagger:route GET /v1/check  Auth Check
	//
	// Check
	//
	// Check Auth
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserRenderResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.Check).Methods(http.MethodGet)
}

func logout(app *App, router *mux.Router) {
	// swagger:route GET /v1/logout  Auth Logout
	//
	// Logout
	//
	// Logout
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: description: empty
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.Logout).Methods(http.MethodGet)
}

func changePassword(app *App, router *mux.Router) {
	// swagger:route POST /v1/changepassword  Auth ChangePasswordForm
	//
	// Change Password
	//
	// Change Password
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserRenderResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.ChangePassword).Methods(http.MethodPost)
}
