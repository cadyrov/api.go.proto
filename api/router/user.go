package router

import (
	"net/http"

	"api.go.proto/internal/core"
	"api.go.proto/internal/domain"
	"github.com/gorilla/mux"
)

// Form CreateUserRequest
//
// swagger:parameters CreateUserRequest
type CreateUserRequest struct {
	// in: body
	// required: true
	Body struct {
		domain.UserForm
	}
}

// Form UpdateUserRequest
//
// swagger:parameters UpdateUserRequest
type UpdateUserRequest struct {
	// ID of updating User
	// Required: true
	// in: path
	UserID string `json:"id"`
	// in: body
	// required: true
	Body struct {
		domain.UserForm
	}
}

// Form DeleteUserRequest
//
// swagger:parameters DeleteUserRequest RestoreUserRequest
type DeleteUserRequest struct {
	// ID of deleting User
	// Required: true
	// in: path
	UserID string `json:"id"`
}

// Form GetUserRequest
//
// swagger:parameters GetUserRequest
type GetUserRequest struct {
	// ID of User
	// Required: true
	// in: path
	UserID string `json:"id"`
}

// Form UpdateUserRole
//
// swagger:parameters UpdateUserRole DeleteUserRole
type UpdateUserRole struct {
	// ID of User
	// Required: true
	// in: path
	UserID string `json:"id"`
	// Roles
	// in: query
	// Required: true
	Roles []string `column:"roles" json:"roles"`
}

// Form SearchUserRequest
//
// swagger:parameters SearchUserRequest
type SearchUserRequest struct {
	// in: body
	// required: true
	Body struct {
		domain.UserSearchForm
	}
}

// Form UploadImgProfile
//
// swagger:parameters UploadImgProfile
type UploadImgProfile struct {
	// in: formData
	// required: true
	//type: file
	File string `json:"file"`
}

// FileRenderResponse response
//
// swagger:response FileRenderResponse
type FileRenderResponse struct {
	// In: body
	Body struct {
		Message string            `json:"message"`
		Data    domain.FileRender `json:"data"`
	}
}

// User response
//
// swagger:response UserResponse
type UserResponse struct {
	// In: body
	Body struct {
		Message string          `json:"message"`
		Data    core.UserRender `json:"data"`
	}
}

// User response
//
// swagger:response UserRenderResponse
type UserRenderResponse struct {
	// In: body
	Body struct {
		Message string          `json:"message"`
		Data    core.UserRender `json:"data"`
	}
}

// UserListResponse response
//
// swagger:response UserListResponse
type UserListResponse struct {
	// In: body
	Body struct {
		Message string             `json:"message"`
		Data    []*core.UserRender `json:"data"`
	}
}

// Form CreateUserForm
//
// swagger:parameters CreateUserForm
type CreateUserForm struct {
	// in: body
	// required: true
	Body struct {
		domain.RegistryForm
	}
}

func (app *App) SetUserRoutes(router *mux.Router) {
	createUser(app, router)
	updateUser(app, router)
	deleteUser(app, router)
	getUser(app, router)
	searchUser(app, router)
	addUserRole(app, router)
	delUserRole(app, router)
	restoreUser(app, router)
	uploadImgProfile(app, router)
}

func uploadImgProfile(app *App, router *mux.Router) {
	// swagger:route POST /v1/user/img User UploadImgProfile
	//
	// Upload profileImg
	//
	// Upload profileImg
	//
	//     Consumes:
	//     - multipart/form-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: FileRenderResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/img", app.UploadImg).Methods(http.MethodPost)
}

func createUser(app *App, router *mux.Router) {
	// swagger:route POST /v1/user/create User CreateUserForm
	//
	// Create
	//
	// Create User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/create", app.CreateUser).Methods(http.MethodPost)
}

func updateUser(app *App, router *mux.Router) {
	// swagger:route PATCH /v1/user/{id} User UpdateUserRequest
	//
	// Update
	//
	// Update User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}", app.UpdateUser).Methods(http.MethodPatch)
}

func deleteUser(app *App, router *mux.Router) {
	// swagger:route DELETE /v1/user/{id}  User DeleteUserRequest
	//
	// Delete
	//
	// Delete User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: ResponseMessage
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id}", app.DeleteUser).Methods(http.MethodDelete)
}

func getUser(app *App, router *mux.Router) {
	// swagger:route GET /v1/user/{id}  User GetUserRequest
	//
	// Get
	//
	// Get User
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id}", app.GetUser).Methods(http.MethodGet)
}

func searchUser(app *App, router *mux.Router) {
	// swagger:route POST /v1/user User SearchUserRequest
	//
	// Search
	//
	// Search User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserListResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("", app.SearchUser).Methods(http.MethodPost)
}

func addUserRole(app *App, router *mux.Router) {
	// swagger:route PUT /v1/user/{id}/role User UpdateUserRole
	//
	// Add roles
	//
	// Add User Roles
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}/role", app.AddUserRole).Methods(http.MethodPut)
}

func delUserRole(app *App, router *mux.Router) {
	// swagger:route DELETE /v1/user/{id}/role User DeleteUserRole
	//
	// Delete roles
	//
	// Delete User Roles
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}/role", app.DelUserRole).Methods(http.MethodDelete)
}

func restoreUser(app *App, router *mux.Router) {
	// swagger:route PUT /v1/user/{id} User RestoreUserRequest
	//
	// Restore
	//
	// Restore User
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: UserResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}", app.RestoreUser).Methods(http.MethodPut)
}
