package router

import (
	"net/http"

	"api.go.proto/internal/domain"
	"github.com/gorilla/mux"
)

// Form UpdateAccountRequest
//
// swagger:parameters UpdateAccountRequest
type UpdateAccountRequest struct {
	// ID of updating Account
	// Required: true
	// in: path
	AccountID string `json:"id"`
	// in: body
	// required: true
	Body struct {
		domain.AccountForm
	}
}

// Form GetAccountRequest
//
// swagger:parameters GetAccountRequest
type GetAccountRequest struct {
	// ID of Account
	// Required: true
	// in: path
	AccountID string `json:"id"`
}

// Account response
//
// swagger:response AccountResponse
type AccountResponse struct {
	// In: body
	Body struct {
		Message string               `json:"message"`
		Data    domain.AccountRender `json:"data"`
	}
}

// AccountListResponse response
//
// swagger:response AccountListResponse
type AccountListResponse struct {
	// In: body
	Body struct {
		Message string                  `json:"message"`
		Data    []*domain.AccountRender `json:"data"`
	}
}

func (app *App) SetAccountRoutes(router *mux.Router) {
	updateAccount(app, router)
	getAccount(app, router)
}

func updateAccount(app *App, router *mux.Router) {
	// swagger:route PUT /v1/account/{id} Account UpdateAccountRequest
	//
	// Update
	//
	// Update Account
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: AccountResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id:[0-9]+}", app.UpdateAccount).Methods(http.MethodPut)
}

func getAccount(app *App, router *mux.Router) {
	// swagger:route GET /v1/account/{id}  Account GetAccountRequest
	//
	// Get
	//
	// Get Account
	//
	//     Consumes:
	//     - multipart/for-data
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: https
	//
	//     Responses:
	//       200: AccountResponse
	//       400: ResponseError
	//       401: ResponseError
	//       403: ResponseError
	//       405: ResponseError
	//       406: ResponseError
	//       500: ResponseError
	router.HandleFunc("/{id}", app.GetAccount).Methods(http.MethodGet)
}
