BRANCH_NAME=$1

if [ $# -eq 0 ]; then
  echo "no branch name"
else
  cd /var/api.go.proto
  git pull origin $BRANCH_NAME
  docker-compose up -d --build
fi