#!/bin/bash
FROM golang:1.16-alpine3.12 AS build
RUN apk add git
RUN apk add curl
RUN apk add make
WORKDIR /go/src/apigo/
COPY ./ /go/src/apigo/
RUN go mod vendor
RUN make build
RUN make swaggerlin
RUN make swaggerspec

FROM alpine:latest AS api
EXPOSE 8080
COPY --from=build /go/src/apigo /apigo/
WORKDIR /apigo/
RUN ls ./
ENTRYPOINT ["/apigo/bin/apigo"]
