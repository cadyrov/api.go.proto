//nolint
package tests

import (
	"fmt"
	"testing"

	"api.go.proto/internal/core"
	"api.go.proto/tools/config"
)

func TestJWT(t *testing.T) {
	ss, err := core.CreateJWTToken(config.JWT{
		Issuer:      "cascsacs",
		SecretKey:   "csacsacas",
		KeyLength:   12,
		HourExpired: 1,
	}, 1)

	if err != nil {
		t.Fatal(err)
	}

	fmt.Printf("Token has been created: %s\n", *ss)
}
