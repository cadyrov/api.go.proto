TAG=$(shell git branch | sed -n -e 's/\* //p')
PROJECT = $(shell basename `pwd`)
REGISTRY = api.go.proto

prune:
	docker container prune
	docker image prune

build:
	go build -o ./bin/$(PROJECT) ./cmd/http/main.go
	go build -o ./bin/translator ./cmd/translate/main.go
	go build -o ./bin/migrate ./cmd/migrate/main.go
	go build -o ./bin/codegen ./cmd/codegen/main.go

swaggermac:
	curl -o swaggerapp  -L https://github.com/go-swagger/go-swagger/releases/download/v0.22.0/swagger_darwin_amd64 && chmod +x swaggerapp

swaggerlin:
	curl -o swaggerapp  -L https://github.com/go-swagger/go-swagger/releases/download/v0.22.0/swagger_linux_amd64 && chmod +x swaggerapp

swaggerspec: 
	./swaggerapp generate spec -m -o swagger.json

migrate/up:
	export `cat .env` && ./bin/migrate up 

migrate/down:
	export `cat .env` && ./bin/migrate down 

migrate/create:
	@read -p "Enter migration name: " name; \
	export `cat .env` && ./bin/migrate create $$name

code/domain:
	@read -p "Enter table name: " name; \
	export `cat .env` &&  ./bin/codegen --domain --name=$$name

code/core:
	@read -p "Enter table name: " name; \
	export `cat .env` && ./bin/codegen --core --name=$$name

code/psql:
	@read -p "Enter table name: " name; \
	export `cat .env` &&  ./bin/codegen --psql --name=$$name

code/controller:
	@read -p "Enter table name: " name; \
	export `cat .env` &&  ./bin/codegen --controller --name=$$name

code/route:
	@read -p "Enter table name: " name; \
	export `cat .env` && ./bin/codegen --route --name=$$name

code/crud:
	cat .env && . ./.env &&  \
	@read -p "Enter table name: " name; \
	./bin/codegen --controller=true --route --psql --core --domain --name=$$name

code/tests:
	@read -p "Enter table name: " name; \
	env ENV=local ./bin/codegen --tests=true --name=$$name

translate:
	cat .env && export `cat .env` && ./bin/translator

lint:
	golangci-lint run --print-linter-name --enable-all --exclude dupl
	
run:
	rm ./bin/$(PROJECT); \
	go build -o ./bin/$(PROJECT) ./cmd/http/main.go; \
	cat .env && export `cat .env` && ./bin/$(PROJECT)
run-locale:
	rm ./bin/$(PROJECT); \
	go build -o ./bin/$(PROJECT) ./cmd/http/main.go; \
	cat .env && export `cat .env` && ./bin/$(PROJECT) --web-ssl-sert-path="" --web-port=80 --web-same-site-none=0
test:
	go test ./tests -v
