// Package main api.go.proto
//
// Entry point api.go.proto
//
// Terms Of Service:
//
//
//     Schemes: https
//     Host: api.go.proto
//     BasePath: /api/
//     Version: 1.0.0
//
//     Consumes:
//     - application/json
//     - multipart/form-data
//
//     Produces:
//     - application/json
//     - multipart/form-data
//
//     Security:
//     - cookie:
//
//     SecurityDefinitions:
//     cookie:
//          type: apiKey
//          name: cookie
//          in: header
//
// swagger:meta
package main
