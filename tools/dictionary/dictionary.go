package dictionary

import (
	"fmt"
	"io/ioutil"
	"strconv"

	"api.go.proto/tools/config"
	"api.go.proto/utils"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/rs/zerolog/log"
)

type Dictionary struct {
	dictionaries  map[int]*godict.Dictionary
	defaultLocale godict.Locale
	config        *config.Config
}

func New(defaultLocale godict.Locale, config *config.Config) (d *Dictionary, e goerr.IError) {
	d = &Dictionary{
		dictionaries:  make(map[int]*godict.Dictionary),
		config:        config,
		defaultLocale: defaultLocale,
	}

	e = d.prepareDictionaries()

	return d, e
}

func (d *Dictionary) ByLocale(locale godict.Locale) (dPtr *godict.Dictionary, e goerr.IError) {
	dPtr, ok := d.dictionaries[int(locale)]

	if !ok {
		log.Info().Int("wrong locale ", int(locale))

		dPtr, ok = d.dictionaries[int(d.defaultLocale)]

		if !ok {
			e = goerr.New(fmt.Sprintf("not found dictionaries %d %d", d.defaultLocale, locale))
		}
	}

	return dPtr, e
}

func (d *Dictionary) prepareDictionaries() (e goerr.IError) {
	files, err := ioutil.ReadDir(utils.RootPath() + "/" + d.config.Project.I18nPath)
	if err != nil {
		e = goerr.New("prepare i18n" + err.Error())

		return
	}

	for _, file := range files {
		if file.IsDir() {
			locale, err := strconv.Atoi(file.Name())
			if err != nil {
				e = goerr.New("prepare dictionary" + err.Error())

				return
			}

			vocabular := godict.Dictionary{}

			bt, err := ioutil.ReadFile(utils.RootPath() +
				"/" + d.config.Project.I18nPath + "/" + file.Name() + "/dictionary.yml")
			if err != nil {
				e = goerr.New("prepare dictionary" + err.Error())

				return
			}

			if e = godict.YamlUnmarshal(bt, &vocabular); e != nil {
				return
			}

			d.dictionaries[locale] = &vocabular
		}
	}

	log.Info().Msg("dictionary prepared")

	return e
}
