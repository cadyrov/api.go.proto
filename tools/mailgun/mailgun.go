package mailgun

import (
	"context"
	"time"

	"github.com/cadyrov/goerr"
	"github.com/mailgun/mailgun-go/v4"
)

const multiplier = 10

type Config struct {
	PrivateAPIKey string `long:"mailgun-private-api-key" env:"APIGO_MAILGUN_PRIVATE_API_KEY" required:"true"`
	Domain        string `long:"mailgun-domain" env:"APIGO_MAILGUN_DOMAIN" required:"true"`
	Sender        string `long:"mailgun-sender" env:"APIGO_MAILGUN_SENDER" required:"true"`
}

type MailGun struct {
	MailGun *mailgun.MailgunImpl
	Config
}

func NewMailGunService(config Config) *MailGun {
	return &MailGun{MailGun: mailgun.NewMailgun(config.Domain, config.PrivateAPIKey), Config: config}
}

func (m *MailGun) NewMessage(subject, body, recipientEmail string) *mailgun.Message {
	return m.MailGun.NewMessage(m.Sender, subject, body, recipientEmail)
}

func (m *MailGun) Send(message *mailgun.Message) (
	response, id string, e goerr.IError) {
	messageContext, cancel := context.WithTimeout(context.Background(), multiplier*time.Second)
	defer cancel()

	response, id, err := m.MailGun.Send(messageContext, message)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	return
}
