package i18n

import (
	"fmt"
	"io/ioutil"
	"strconv"

	"api.go.proto/tools/config"
	"api.go.proto/utils"
	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
)

type Translator interface {
	Translatef(godict.Locale, string, ...interface{}) string
}

func New(defaultLocale godict.Locale, config *config.Config) (t Translator, e goerr.IError) {
	mt := &MemoryTranslator{
		defaultLocale: defaultLocale,
		localeMap:     make(map[int]map[string]string),
	}

	e = mt.prepareI18n(config)

	return mt, e
}

type MemoryTranslator struct {
	defaultLocale godict.Locale
	localeMap     map[int]map[string]string
}

func (mt *MemoryTranslator) prepareI18n(config *config.Config) (e goerr.IError) {
	files, err := ioutil.ReadDir(utils.RootPath() + "/" + config.Project.I18nPath)
	if err != nil {
		e = goerr.New("prepare i18n" + err.Error())

		return
	}

	for _, file := range files {
		if file.IsDir() {
			locale, err := strconv.Atoi(file.Name())
			if err != nil {
				e = goerr.New("prepare i18n" + err.Error())

				return
			}

			vocabulary := make(map[string]string)

			bt, err := ioutil.ReadFile(utils.RootPath() + "/" + config.Project.I18nPath + "/" + file.Name() + "/data.yml")
			if err != nil {
				e = goerr.New("prepare i18n" + err.Error())

				return
			}

			err = yaml.Unmarshal(bt, &vocabulary)
			if err != nil {
				e = goerr.New("prepare i18n" + err.Error())

				return
			}

			mt.localeMap[locale] = vocabulary
		}
	}

	log.Info().Msg("i18n prepared")

	return e
}

func (mt *MemoryTranslator) translate(locale godict.Locale, value string) string {
	if locale == 0 {
		locale = mt.defaultLocale
	}

	mp, ok := mt.localeMap[int(locale)]
	if !ok {
		log.Info().Int("vocabulary not found", int(locale))

		return value
	}

	result, ok := mp[value]
	if !ok {
		log.Info().Str("word not found", value).Int("locale", int(locale))

		return value
	}

	return result
}

func (mt *MemoryTranslator) Translatef(locale godict.Locale, key string, val ...interface{}) string {
	return fmt.Sprintf(mt.translate(locale, key), val...)
}

func (mt *MemoryTranslator) TranslateErr(locale godict.Locale, err goerr.IError) goerr.IError {
	arrErr := err.GetDetails()

	for i := range arrErr {
		arrErr = append(arrErr, mt.TranslateErr(locale, arrErr[i]))
	}

	e := goerr.New(mt.translate(locale, err.GetMessage())).HTTP(err.GetCode()).SetID(err.GetID())

	for i := range arrErr {
		e.PushDetail(arrErr[i])
	}

	return e
}
