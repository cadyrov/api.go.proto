package config

import (
	"time"

	"api.go.proto/tools/mailgun"
	"github.com/cadyrov/godict"
)

type Config struct {
	Project
	PSQL
	Generator
	JWT
	Web
	Mailgun mailgun.Config
	S3
}

type Project struct {
	Name          string        `long:"project-name" env:"APIGO_PROJECT_NAME" required:"true"`
	Debug         bool          `long:"project-debug" env:"APIGO_PROJECT_DEBUG" required:"true"`
	MigratePath   string        `long:"project-migrate-path" env:"APIGO_PROJECT_MIGRATE_PATH" required:"true"`
	SwaggerPath   string        `long:"project-swagger-path" env:"APIGO_PROJECT_SWAGGER_PATH" required:"true"`
	I18nPath      string        `long:"project-i18n-path" env:"APIGO_PROJECT_I18N_PATH" required:"true"`
	DefaultLocale godict.Locale `long:"project-default-locale" env:"APIGO_PROJECT_DEFAULT_LOCALE" required:"true"`
	URLRegistry   string        `long:"project-url-registry" env:"APIGO_PROJECT_URL_REGISTRY" required:"true"`
}

type JWT struct {
	Issuer      string `long:"jwt-issuer" env:"APIGO_JWT_ISSUER"`
	SecretKey   string `long:"jwt-secret-key" env:"APIGO_JWT_SECRET_KEY"`
	KeyLength   int    `long:"jwt-key-length" env:"APIGO_JWT_KEY_LENGTH"`
	HourExpired int    `long:"jwt-hour-expired" env:"APIGO_JWT_HOUR_EXPIRED"`
}

type Web struct {
	Host          string        `long:"web-host" env:"APIGO_WEB_HOST"`
	Port          int           `long:"web-port" env:"APIGO_WEB_PORT"`
	ReadTimeout   int           `long:"web-read-timeout" env:"APIGO_WEB_READ_TIMEOUT"`
	WriteTimeout  int           `long:"web-write-timeout" env:"APIGO_WEB_WRITE_TIMEOUT"`
	IdleTimeout   int           `long:"web-idle-timeout" env:"APIGO_WEB_IDLE_TIMEOUT"`
	CookieExpired time.Duration `long:"web-cookie-expired" env:"APIGO_WEB_COOKIE_EXPIRED"`
	Cors          string        `long:"web-cors" env:"APIGO_WEB_CORS"`
	SSLSertPath   string        `long:"web-ssl-sert-path" env:"APIGO_WEB_SSL_SERT_PATH"`
	SSLKeyPath    string        `long:"web-ssl-web" env:"APIGO_WEB_SSL_KEY_PATH"`
	CookieName    string        `long:"web-cookie-name" env:"APIGO_WEB_COOKIE_NAME"`
	SameSiteNone  int           `long:"web-same-site-none" env:"APIGO_WEB_SAME_SITE_NONE"`
}

//nolint
type Generator struct {
	DomainPath               string `long:"generator-domain-path" env:"APIGO_GENERATOR_DOMAIN_PATH"`
	DomainTemplatePath       string `long:"generator-domain-template-path" env:"APIGO_GENERATOR_DOMAIN_TEMPLATE_PATH"`
	PSQLPath                 string `long:"generator-psql-path" env:"APIGO_GENERATOR_PSQL_PATH"`
	PSQLTemplatePath         string `long:"generator-psql-template-path" env:"APIGO_GENERATOR_PSQL_TEMPLATE_PATH"`
	CorePath                 string `long:"generator-core-path" env:"APIGO_GENERATOR_CORE_PATH"`
	CoreTemplatePath         string `long:"generator-core-template-path" env:"APIGO_GENERATOR_CORE_TEMPLATE_PATH"`
	RoutePath                string `long:"generator-route-path" env:"APIGO_GENERATOR_ROUTE_PATH"`
	RouteTemplatePath        string `long:"generator-route-template-path" env:"APIGO_GENERATOR_ROUTE_TEMPLATE_PATH"`
	ControllerPath           string `long:"generator-controller-path" env:"APIGO_GENERATOR_CONTROLLER_PATH"`
	ControllerTemplatePath   string `long:"generator-cotroller-template-path" env:"APIGO_GENERATOR_CONTROLLER_TEMPLATE_PATH"`
	TestEntitiesPath         string `long:"generator-test-entities-path" env:"APIGO_GENERATOR_TEST_ENTITIES_PATH"`
	TestEntitiesTemplatePath string `long:"generator-test-entties-template-path" env:"APIGO_GENERATOR_TEST_ENTITIES_TEMPLATE_PATH"`
	CoreTestsPath            string `long:"generator-core-test-path" env:"APIGO_GENERATOR_CORE_TEST_PATH"`
	CoreTestsTemplatePath    string `long:"generator-core-test-template-path" env:"APIGO_GENERATOR_CORE_TEST_TEMPLATE_PATH"`
}

type PSQL struct {
	Host           string `long:"repository-psql-host" env:"APIGO_PSQL_HOST"`
	Port           int    `long:"repository-psql-port" env:"APIGO_PSQL_PORT"`
	UserName       string `long:"repository-psql-user-name" env:"APIGO_PSQL_USER_NAME"`
	DBName         string `long:"repository-psql-db-name" env:"APIGO_PSQL_DB_NAME"`
	Password       string `long:"repository-psql-password" env:"APIGO_PSQL_PASSWORD"`
	SslMode        string `long:"repository-psql-ssl-mode" env:"APIGO_PSQL_SSL_MODE"`
	Binary         bool   `long:"repository-psql-binary" env:"APIGO_PSQL_BINARY"`
	MaxConnections int    `long:"repository-psql-max-connections" env:"APIGO_PSQL_MAX_CONNECTIONS"`
	ConnectionIdle int    `long:"repository-psql-connection-idle" env:"APIGO_PSQL_CONNECTION_IDLE"`
}

type S3 struct {
	Endpoint  string `long:"s3-endpoint" env:"APIGO_S3_ENDPOINT"`
	AccessKey string `long:"s3-access=key" env:"APIGO_S3_ACCESS_KEY"`
	SecretKey string `long:"s3-secret-key" env:"APIGO_S3_SECRET_KEY"`
	Bucket    string `long:"s3-bucket" env:"APIGO_S3_BUCKET"`
}
