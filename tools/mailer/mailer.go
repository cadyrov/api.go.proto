package mailer

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/mail"
	"net/smtp"

	"github.com/cadyrov/goerr"
	"github.com/rs/zerolog/log"
)

type Mailer struct {
	config Config
	auth   smtp.Auth
}

type Config struct {
	User     string `long:"mailer-user" env:"APIGO_MAILER_USER" required:"true"`
	Password string `long:"mailer-password" env:"APIGO_MAILER_PASSWORD" required:"true"`
	SMTP     string `long:"mailer-smtp" env:"APIGO_MAILER_SMTP" required:"true"`
	Port     string `long:"mailer-port" env:"APIGO_MAILER_PORT" required:"true"`
}

func New(cnf Config) *Mailer {
	m := &Mailer{config: cnf}
	m.auth = smtp.PlainAuth("", m.config.User, m.config.Password, m.config.SMTP)

	return m
}

func (m *Mailer) Send(to string, subject string, msg []byte) goerr.IError {
	fromAddr, toAddr, message := m.prepareMessage(to, subject, msg)

	servername := m.config.SMTP + ":" + m.config.Port

	host, _, _ := net.SplitHostPort(servername)

	// #nosec
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		return m.toError(err)
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		return m.toError(err)
	}

	if err = c.Auth(m.auth); err != nil {
		return m.toError(err)
	}

	if err = c.Mail(fromAddr.Address); err != nil {
		return m.toError(err)
	}

	if err = c.Rcpt(toAddr.Address); err != nil {
		return m.toError(err)
	}

	w, err := c.Data()
	if err != nil {
		return m.toError(err)
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		return m.toError(err)
	}

	err = w.Close()
	if err != nil {
		return m.toError(err)
	}

	if err = c.Quit(); err != nil {
		return m.toError(err)
	}

	return nil
}

func (m *Mailer) prepareMessage(to string, subject string,
	msg []byte) (fromAddr mail.Address, toAddr mail.Address, message string) {
	fromAddr = mail.Address{Address: m.config.User}
	toAddr = mail.Address{Address: to}

	headers := make(map[string]string)
	headers["From"] = fromAddr.String()
	headers["To"] = toAddr.String()
	headers["Subject"] = subject

	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	message += "\r\n" + string(msg)

	return fromAddr, toAddr, message
}

func (m *Mailer) toError(err error) goerr.IError {
	e := goerr.New(err.Error())

	log.Error().Err(e)

	return e
}
