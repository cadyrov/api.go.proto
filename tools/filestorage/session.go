package filestorage

import (
	"io"

	"api.go.proto/tools/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/cadyrov/goerr"
)

type S3Runner struct {
	S3Config config.S3
	Session  *s3.S3
}

func NewS3Runner(s3 config.S3) *S3Runner {
	r := &(S3Runner{
		S3Config: s3,
	})

	r.Session = r.AWSSession()

	return r
}

func (r *S3Runner) AWSUpload(file io.Reader, name string) (output *s3manager.UploadOutput, e goerr.IError) {
	uploader := s3manager.NewUploaderWithClient(r.Session)

	upParams := &s3manager.UploadInput{
		Bucket: &r.S3Config.Bucket,
		Key:    aws.String(name),
		Body:   file,
	}

	output, err := uploader.Upload(upParams)
	if err != nil {
		e = goerr.New(err.Error())
	}

	return output, e
}

func (r *S3Runner) AWSDelete(name string) (e goerr.IError) {
	_, err := r.Session.DeleteObject(&s3.DeleteObjectInput{
		Bucket: &r.S3Config.Bucket,
		Key:    aws.String(name),
	})
	if err != nil {
		e = goerr.New(err.Error())
	}

	return e
}

func (r *S3Runner) AWSList() (list *s3.ListObjectsOutput, e goerr.IError) {
	list, err := r.Session.ListObjects(&s3.ListObjectsInput{
		Bucket:    &r.S3Config.Bucket,
		Delimiter: aws.String("/"),
	})
	if err != nil {
		e = goerr.New(err.Error())
	}

	return
}

func (r *S3Runner) AWSSession() *s3.S3 {
	force := true

	sess := session.Must(session.NewSession(&aws.Config{
		Region:           aws.String(""),
		Endpoint:         &r.S3Config.Endpoint,
		Credentials:      credentials.NewStaticCredentials(r.S3Config.AccessKey, r.S3Config.SecretKey, ""),
		S3ForcePathStyle: &force,
	}))

	s3Svc := s3.New(sess, &aws.Config{
		Region: aws.String(r.S3Config.Endpoint),
		Credentials: credentials.NewStaticCredentials(
			r.S3Config.AccessKey,
			r.S3Config.SecretKey,
			"",
		),
	})

	return s3Svc
}
